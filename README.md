### 这周进度：
1）研究了Elongation of Curvature-Bounded Path、The Classification of Homotopy Classes of Bounded Curvature Paths两篇论文，基本认为断续区域的下界为Ω区域的边界。（即采用将直线段变成圆的方式扩展路径长度，可以得到最大下界）
2）认识到了当CCC为最短路径时，CCC形式路径是连续的；但是当最短路径形式不是CCC时，CCC路径不一定连续
### 安排：
1）继续寻找断续区域的上界。
2）分析不同情况下的路径下界情况
