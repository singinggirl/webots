import cv2
import numpy as np

#获取陀螺仪的数值并把他转化为角度
#陀螺仪返回的数值在0到1024之间，对应的数值在-1600[deg/sec]和+1600[deg/sec]之间，与真实机器人返回的数值类似
def AngleGet(mGyro, mTimeStep,angle):
	init_angle = mGyro.getValues() #初始信号
	angle_turn = np.array(init_angle)/1024*3200-1600 #转化成角度
	angle += angle_turn * mTimeStep/1000 #积分获取角度
	return angle


#摄像头数据
def ImageGet(mCamera, mTimeStep):
	image = mCamera.getImage() #获取图片速度比getImageArray快
	height,width = mCamera.getHeight(), mCamera.getWidth()
	image = np.frombuffer(image, np.uint8).reshape((height, width, 4))
	return image
    
