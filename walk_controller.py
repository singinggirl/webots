"""walk_controller controller."""
#北京理工大学烤地瓜队
# -*- coding: utf-8 -*-
#定义关卡：0-绿地上下开合栏杆 1-绿色回字陷阱 2-灰(棕、白)色扫雷关 3-蓝白棕地障碍物 4-黑色棕(白、灰)地窄门 5-绿色独木桥 6-白(棕、灰)底踢球进洞 7-蓝绿红台阶 8-水平开合栏杆(花纹地)
#地雷(2)、门(4)、球(6)三关地面会交换
import os
import sys
import numpy as np
import cv2
import math
from itertools import count,repeat
libraryPath = os.path.join(os.environ.get("WEBOTS_HOME"), 'projects', 'robots', 'robotis', 'darwin-op', 'libraries',  'python37')  
libraryPath = libraryPath.replace('/', os.sep)  
sys.path.append(libraryPath)  
from managers import RobotisOp2GaitManager, RobotisOp2MotionManager  
from controller import Robot,Motion
import sensorData
from utils import *

#赛道材料
raceMaterialInfo = [
    {'material':'grass','possible_stage':[0],'hsv':{'low':[25,88,90],'high':[50,210,200]}},
    {'material':'grey','possible_stage':[2,4,6],'hsv':{'low':[17,4,160],'high':[54,20,210]}},
    {'material':'yellow_brick','possible_stage':[2,4,6,7],'hsv':{'low':[16,107,61],'high':[31,255,227]}}, 
    {'material':'green','possible_stage':[1,5],'hsv':{'low':[46,36,50],'high':[92,251,255]}},
    {'material':'white','possible_stage':[2,4,6],'hsv':{'low':[10, 5, 200],'high':[33, 29, 242]}},
    {'material':'blue_flower','possible_stage':[8],'hsv':{'low':[100, 10, 100],'high':[150, 80, 200]}},
]

#关卡
stageInfo = {
    0 : {'name':'上下横杆','materialInfo':[raceMaterialInfo[0]]}, 
    1 : {'name':'回字陷阱','materialInfo':[raceMaterialInfo[3]]}, 
    2 : {'name':'扫雷','materialInfo':[raceMaterialInfo[1],raceMaterialInfo[2],raceMaterialInfo[4]]}, 
    3 : {'name':'障碍','materialInfo':[]}, 
    4 : {'name':'窄桥','materialInfo':[raceMaterialInfo[3]]}, 
    5 : {'name':'踢球','materialInfo':[raceMaterialInfo[1],raceMaterialInfo[2],raceMaterialInfo[4]]}, 
    6 : {'name':'楼梯','materialInfo':[raceMaterialInfo[2]]}, 
    7 : {'name':'水平开合横杆','materialInfo':[raceMaterialInfo[5]]}, 
    8 : {'name':'窄门','materialInfo':[raceMaterialInfo[1],raceMaterialInfo[2],raceMaterialInfo[4]]}, 
}

class Walk():
    def __init__(self):  
        self.robot = Robot()  # 初始化Robot类以控制机器人  
        self.mTimeStep = int(self.robot.getBasicTimeStep())  # 获取当前每一个仿真步所仿真时间mTimeStep  
        self.HeadLed = self.robot.getDevice('HeadLed')  # 获取头部LED灯  
        self.EyeLed = self.robot.getDevice('EyeLed')  # 获取眼部LED灯  
        self.HeadLed.set(0xff0000)  # 点亮头部LED灯并设置一个颜色  
        self.EyeLed.set(0xa0a0ff)  # 点亮眼部LED灯并设置一个颜色  
        self.mAccelerometer = self.robot.getDevice('Accelerometer')  # 获取加速度传感器  
        self.mAccelerometer.enable(self.mTimeStep)  # 激活传感器，并以mTimeStep为周期更新数值  
        
        self.fup = 0
        self.fdown = 0  # 定义两个类变量，用于之后判断机器人是否摔倒 
        self.isObstacle = False
        self.mGyro = self.robot.getDevice('Gyro')  # 获取陀螺仪  
        self.mGyro.enable(self.mTimeStep)  # 激活陀螺仪，并以mTimeStep为周期更新数值
                
        self.mCamera = self.robot.getDevice('Camera') #获取相机
        self.mCamera.enable(self.mTimeStep) #激活相机，并以mTimeStep为周期更新数值
        
        self.positionSensors = []  # 初始化关节角度传感器  
        self.positionSensorNames = ('ShoulderR', 'ShoulderL', 'ArmUpperR', 'ArmUpperL',  
                                    'ArmLowerR', 'ArmLowerL', 'PelvYR', 'PelvYL',  
                                     'PelvR', 'PelvL', 'LegUpperR', 'LegUpperL',  
                                     'LegLowerR', 'LegLowerL', 'AnkleR', 'AnkleL',  
                                     'FootR', 'FootL', 'Neck', 'Head')  # 初始化各传感器名  
                                     
       # 获取各传感器并激活，以mTimeStep为周期更新数值  
        for i in range(0, len(self.positionSensorNames)):  
            self.positionSensors.append(self.robot.getDevice(self.positionSensorNames[i] + 'S'))  
            self.positionSensors[i].enable(self.mTimeStep)  
       #电机
        self.motors = []
        self.motorNames = ('ShoulderR', 'ShoulderL', 'ArmUpperR', 'ArmUpperL',
                           'ArmLowerR', 'ArmLowerL', 'PelvYR', 'PelvYL',
                           'PelvR', 'PelvL', 'LegUpperR', 'LegUpperL',
                           'LegLowerR', 'LegLowerL', 'AnkleR', 'AnkleL',
                           'FootR', 'FootL', 'Neck', 'Head')
        for i in range(0, len(self.motorNames)):
            self.motors.append(self.robot.getDevice(self.motorNames[i]))
            
        self.mKeyboard = self.robot.getKeyboard()  # 初始化键盘读入类  
        self.mKeyboard.enable(self.mTimeStep)  # 以mTimeStep为周期从键盘读取  
        
        self.mMotionManager = RobotisOp2MotionManager(self.robot)  # 初始化机器人动作组控制器  
        self.mGaitManager = RobotisOp2GaitManager(self.robot,  "config.ini")  # 初始化机器人步态控制器
        
        #初始化
        self.angle = np.array([0.,0.,0.]) #陀螺仪获得的角度  
        self.vx=0.
        self.vy=0.
        self.va=0.
        self.stageLeft={0,1,2,3,4,5,6,7,8}
        self.materialInfo ={'material':'grass','possible_stage':[0],'hsv':{'low':[25,88,90],'high':[50,210,200]}}
        self.stagenow = 0
        self.isObstacle = False
        self.corner = 0

    def wait(self, ms):  
        startTime = self.robot.getTime()  
        s = ms / 1000.0  
        while (s + startTime >= self.robot.getTime()):  
            self.myStep() 

 
    def myStep(self):  
        ret = self.robot.step(self.mTimeStep) 
       # self.angle = self.mGyro.getValues()
        self.angle=sensorData.AngleGet(self.mGyro,self.mTimeStep,self.angle)
        if ret == -1:  
            exit(0) 
            
    def MotionSet(self, v_x=0., v_y=0., v_a=0.):
         self.vx = rangeLimit(v_x, 1, -1)
         self.vy = rangeLimit(v_y, 1, -1)
         self.va = rangeLimit(v_a, 1, -1) #貌似官网资料有误
         self.mGaitManager.setXAmplitude(self.vx)  # 设置x向速度
         self.mGaitManager.setYAmplitude(self.vy)  # 设置y向速度
         self.mGaitManager.setAAmplitude(self.va)  # 设置转动速度
    
    #开始！
    def start(self):
        self.myStep()  # 仿真一个步长，刷新传感器读数  
        self.mMotionManager.playPage(9)  # 执行动作组9号动作，初始化站立姿势，准备行走  
        self.wait(200)  # 等待200ms 
        self.mGaitManager.setBalanceEnable(True)  #更稳定
        self.mGaitManager.start()  # 步态生成器进入行走状态  
        self.wait(200)
        print('机器人准备就绪！')
        
     #机器人走偏时进行矫正   
    def StraightenRobot(self, therehold=6, eps=1,angle_wanted=0):
        if np.abs(self.angle[-1])>therehold:
            print("现在的偏航角度是:%.3f"%self.angle[2])
            print("---偏离航线！需要矫正---")
            print("----开始矫正-----")
            while np.abs(self.angle[2]) > eps:
                change_speed = -1024/32000 * self.angle[-1]
                change_speed = rangeLimit(change_speed, 1,-1)
                self.MotionSet(self.vx,self.vy, change_speed)
                self.mGaitManager.step(self.mTimeStep)
                self.myStep()
            print("现在的偏航角度是:%.3f"%self.angle[2])
            print("矫正完成!")

#检测是否太靠近边缘
    
    def CheckRound(self,img,materialInfo):
        hsv = cv2.cvtColor(img,cv2.COLOR_BGR2HSV)  #转化为hsv
        low = np.array(materialInfo ['hsv']['low'])
        high = np.array(materialInfo ['hsv']['high'])  
        mask=cv2.inRange(hsv,low,high)
        mask = cv2.dilate(mask, np.ones((3, 3), np.uint8), iterations=2)
        mask = cv2.erode(mask, None, iterations=2)  
        road = np.where(mask==255)[0]
        num = len(road)
        print("距离边缘长度")
        print(num)
        if num < 10000 and np.abs(self.angle[-1])>20:
            return True
        else:
            return False

    def setDevice(self, number, position, *speed):
        if len(speed):
           self.motors[number].setVelocity(speed[0])
        else:
           self.motors[number].setVelocity(1)
        self.motors[number].setPosition(position)       
    
    def stand(self):
        self.setDevice(10,0,1)
        self.setDevice(11, 0, 1)
        self.setDevice(12, 0, 2)
        self.setDevice(13, 0, 2)
        self.setDevice(14, -0.1, 1)
        self.setDevice(15, 0.1, 1)
        self.myStep()
        self.wait(1000)    
       
      
    def stage0(self):
        print("----上下横杆开始-----")
        #方案：观察视野中有没有明黄色，有，等待它消失；没有，等他出现以后消失再走
        isWalk = False #是否可以走动
        BarkDown = False
        DownTime = 0  #落下次数
        iterator = count(start=0,step=1) #迭代生成时间序列
        for n in iterator:
        #开始前先矫正
            if n % 10 == 0:
                self.StraightenRobot()
                self.mGaitManager.step(self.mTimeStep)  #这里得单独加上，不然不会刷新，会一直呆在原地
                self.myStep()
                image1 =sensorData.ImageGet(self.mCamera, self.mTimeStep)   #获取图片
                hsv = cv2.cvtColor(image1,cv2.COLOR_BGR2HSV)  #转化为hsv
                low = np.array([5,198,200])
                high = np.array([95,251,255])
                mask = cv2.inRange(hsv, low, high) #提取黄色
                #cv2.imwrite('/home/wenjing/test.png', mask)
                contours,_ = cv2.findContours(mask, cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
                count1 = 0 #要注意当后面为黄色背景时会产生问题
                for contour in contours:
                    if cv2.contourArea(contour) > 100:
                        count1 += 1
                if count1 > 2:
                        BarDown = True
                        DownTime = 1
                else:
                        BarDown = False
                if DownTime == 0:
                    print("---横杆未落下，等待下一次开启----")
                    isWalk = False
                elif DownTime == 1:
                    if BarDown == True:
                        print("---横杆未开启,等待开启---")
                        isWalk = False
                    else:
                        print("----可以开始走动了-----")
                        isWalk = True
            if isWalk:
                print("调整速度!")
                self.MotionSet(v_x=1.)
               # print(self.vx)
                break
            self.mGaitManager.step(self.mTimeStep)
            self.myStep()      
        #判断是否结束
       # print(self.positionSensors[19].getValue())
        print("---接下来判断机器人有没有走出去!---")
        time = count(start=0,step=1)
        for n in time:
            self.StraightenRobot()
            self.mGaitManager.step(self.mTimeStep)  #这里得单独加上，不然不会刷新，会一直呆在原地
            self.myStep()
            if n % 10 == 0 :
                image1 =sensorData.ImageGet(self.mCamera, self.mTimeStep)   #获取图片
                hsv = cv2.cvtColor(image1,cv2.COLOR_BGR2HSV)  #转化为hsv
                low = np.array(self.materialInfo ['hsv']['low'])
                high = np.array(self.materialInfo ['hsv']['high'])
                image_half = hsv[self.mCamera.getHeight()//2:,:]   
                #cv2.imwrite('/home/wenjing/test2.png', image_half)      
                mask=cv2.inRange(image_half,low,high)
                road = np.where(mask==255)[0]
                num = len(road)                
                if num < 500 or n > 1000:
                    break
        self.stageLeft.remove(0)
       # print(self.stageLeft)
        print("------------上下横杆关结束-----------")
                        
    def stage1(self):    
        print("----绿色回字桥开始!------")
       #当中间陷阱出现较近时，左转，走到 边缘再右转，再到边缘再右转，到中间差不多左转 
       #识别中间洞的左下顶点  
       # print(self.positionSensors[19].getValue())
        self.motors[19].setPosition(-0.1)  #为了防止机器人看到左下角的点时距离太近
        self.MotionSet(v_x = 0.)
       # print(self.positionSensors[19].getValue())
       # turn
        while True:
            change_speed = -0.2 * self.angle[-1]
            change_speed = rangeLimit(change_speed, 1,-1)
            self.MotionSet(v_x = 0.5,v_y = 0., v_a=change_speed)  
            self.mGaitManager.step(self.mTimeStep)  #这里得单独加上，不然不会刷新，会一直呆在原地
            self.myStep()    
            if np.abs(self.angle[-1])<1:
                break                   
        isTurn = False
        iterator = count(start=0,step=1) #迭代生成时间序列        
        for n in iterator:
                self.StraightenRobot()
                self.mGaitManager.step(self.mTimeStep)  #这里得单独加上，不然不会刷新，会一直呆在原地
                self.myStep()                
                if n%5 == 0 and np.abs(self.angle[-1])<1:
                    image1 =sensorData.ImageGet(self.mCamera, self.mTimeStep)   #获取图片
                    hsv = cv2.cvtColor(image1,cv2.COLOR_BGR2HSV)  #转化为hsv
                    hsv_gaussian = cv2.GaussianBlur(hsv, (3, 3), 0)
                    low = np.array([46,36,50])
                    high = np.array([88,245,255])
                    mask  = cv2.inRange(hsv_gaussian[self.mCamera.getHeight()//2:,:] ,low,high)
                    mask = cv2.dilate(mask, np.ones((3, 3), np.uint8), iterations=2)
                    mask = cv2.erode(mask, None, iterations=2)                   
                    counters,_= cv2.findContours(mask,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
                    cv2.imwrite('/home/wenjing/test3.png', mask)    
                    for contour in counters:
                        # 矩形拟合
                        x, y, w, h = cv2.boundingRect(contour) 
                     #   print(w*h)
                    #    print(w)
                   #     print(h)
                        #防止阴影的干扰,最低点出现在1/3时转弯
                        if h < self.mCamera.getHeight()//2 and (y+h) >self.mCamera.getHeight()/8 and w > 0.8*h:
                            print("是时候转弯了!")
                            isTurn = True
                            break
                    if isTurn:
                        break
        self.motors[19].setPosition(0)  
        print("第一次转向")
        self.motors[19].setPosition(-0.2)
        isTurn = False
        iterator = count(start=0,step=1) #迭代生成时间序列
        for n in iterator:
            change_speed = -1024/32000 * (self.angle[2]+90)
            change_speed =rangeLimit(change_speed, 1,-1)
            self.MotionSet(v_x = 1.,v_a=change_speed)
            self.mGaitManager.step(self.mTimeStep)
            self.myStep()
            #判断是否靠近边缘
            if n % 10 == 0 :
                image1 =sensorData.ImageGet(self.mCamera, self.mTimeStep)   #获取图片
                hsv = cv2.cvtColor(image1,cv2.COLOR_BGR2HSV)  #转化为hsv
                low = np.array([50, 36, 50])
                high = np.array([88, 245, 255])
                image_half = hsv[self.mCamera.getHeight()//2:,:]                   
                mask=cv2.inRange(image_half,low,high) 
                cv2.imwrite('/home/wenjing/test4.png', mask)  
                road = np.where(mask==255)[0]
                num = len(road)                
                if num < 40 :
                    print("该第二次转弯了!")
                    isTurn = True	  
                    self.MotionSet(v_x =0.)     
                    break
     #   print(isTurn)     
        if isTurn == True:
            self.StraightenRobot()
            self.mGaitManager.step(self.mTimeStep)
            self.myStep()
            #接着走窄桥,当右半边 没有绿色时
        time = count(start=0,step=1)
        self.mGaitManager.setXAmplitude(1.0) 
        self.vx=1.0
        for n in time:
            self.StraightenRobot()
            self.mGaitManager.step(self.mTimeStep)  
            self.myStep()#这里得单独加上，不然不会刷新，会一直呆在原地
            if n % 10 == 0 :
                image1 =sensorData.ImageGet(self.mCamera, self.mTimeStep)   #获取图片
                hsv = cv2.cvtColor(image1,cv2.COLOR_BGR2HSV)  #转化为hsv
                low =np.array([50,36 ,50])
                high = np.array([88, 245, 255])
                image_half = hsv[:,self.mCamera.getWidth()//2:]                         
                mask=cv2.inRange(image_half,low,high)
                road = np.where(mask==255)[0]
                cv2.imwrite('/home/wenjing/test5.png', mask)
                num = len(road)   
        #        print(num)             
                if num < 500 :
                    print("窄桥结束!")
                    break
            #由于可能下一关是绿色独木桥，因此直接写死
        self.motors[19].setPosition(0.0)
        print("斜走")                              
        while np.abs(self.angle[-1]-45)>0.5:
                change_speed = -1024/32000 * (self.angle[-1] - 45)
                change_speed = rangeLimit(change_speed, 1,-1)
                self.MotionSet(v_x=1.0,v_a=change_speed)
                self.mGaitManager.step(self.mTimeStep)
                self.myStep()
       # self.mGaitManager.setXAmplitude(1.0)  
        self.MotionSet(v_a = 0.)  # 设置转动速度
        self.StraightenRobot()
        self.mGaitManager.step(self.mTimeStep)
        self.myStep() 
        self.stageLeft.remove(1)
        print("----回字结束!----")

    def stage2(self):
        print("----地雷关-----")
        #寻找地雷
        self.motors[19].setPosition(-0.4)
        self.MotionSet(v_x = 1.0)
        time = count(start=0,step=1)
        turnLeft = 0
        turnRight = 0
        turnFlag = "no"
        rightValue = 0
        leftValue = 0
        Oblique  = 1  #是否斜走
        OutFlag = False
        for n in time:
            self.mGaitManager.step(self.mTimeStep)  #这里得单独加上，不然不会刷新，会一直呆在原地
            self.myStep()                
            if n%5 == 0 and np.abs(self.positionSensors[19].getValue()+0.4)<0.05:
                img =sensorData.ImageGet(self.mCamera, self.mTimeStep)   #获取图片
                Flag = self.CheckRound(img, self.materialInfo)
                img_gray= cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)  #灰度化
                ret, binary_mine = cv2.threshold(img_gray, 10, 255, cv2.THRESH_BINARY_INV)
                cv2.imwrite("/home/wenjing/mine.png",binary_mine)
                contours, h = cv2.findContours(binary_mine, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
                #如果视野中的地雷大于两个，选择距离机器人最近的两个
                mine = []
                for contour in contours:
                    if cv2.contourArea(contour) > 5:
                        M = cv2.moments(contour)  
                        center_x = int(M["m10"] / M["m00"])
                        center_y = int(M["m01"] / M["m00"])     #计算中心
                        if (self.mCamera.getWidth()/6 < center_x <5 * self.mCamera.getWidth()/6) and (self.mCamera.getHeight()/4)< center_y :
                            mine.append((center_x,center_y))   
                #选择路标地雷<=2个
                mine_use  = []
                if len(mine) >= 2:
                    mine_use = sorted(mine,key = lambda x:x[1])[-2:]
                    print("---两个地雷-----")
                elif len(mine) == 1:
                    mine_use = mine
                    print("-----一个地雷-----")
                    print(Oblique)
                elif len(mine) == 0:
                    mine_use = []
   
                if (len(mine_use)) and Oblique == 0  :
                     Oblique = 1
                     print("Oblique:%.3f"%Oblique)
                if (len(mine_use)) and Oblique==1:
                     #如果地雷离得太近,需要后退
                     self.motors[18].setPosition((math.radians(0))) 
                     print("mine:%.3f"%mine_use[0][1])
                     if mine_use[0][1] > 100:
                          print("距离过近!")
                          change_speed = -1024/32000 * (self.angle[2])
                          change_speed =rangeLimit(change_speed, 1,-1)
                          self.MotionSet(v_x = -1.,v_a=change_speed)
                          self.mGaitManager.step(self.mTimeStep)
                          self.myStep() 
                          continue
                      #面对合适距离的地雷,判断该往哪边转躲避地雷
                     elif mine_use[0][1] <= 100:
                          print("开始避雷!")
                          self.vx = 0
                          self.mGaitManager.setXAmplitude(0.0) #为什么有的时候不太灵敏呢？
                          self.mGaitManager.step(self.mTimeStep)
                          self.myStep()
                          
                          #先看左边
                          print("机器人脑袋左转")                          
                          self.motors[18].setPosition(math.radians(90))
                          self.mGaitManager.setXAmplitude(0.0) #为什么有的时候不太灵敏呢？
                          self.mGaitManager.step(self.mTimeStep)
                          self.myStep()
                          self.StraightenRobot()
                          self.mGaitManager.step(self.mTimeStep)
                          self.myStep()    
                          #好像关节不是很灵敏                                              
                          while True:
                              self.mGaitManager.step(self.mTimeStep)
                              self.myStep() 
                              print(self.positionSensors[18].getValue())
                              if (np.abs(self.positionSensors[18].getValue()-math.radians(90))<0.1):
                                  image1 =sensorData.ImageGet(self.mCamera, self.mTimeStep)   #获取图片
                                  hsv = cv2.cvtColor(image1,cv2.COLOR_BGR2HSV)  #转化为hsv
                                  low =np.array(self.materialInfo ['hsv']['low'])
                                  high = np.array(self.materialInfo['hsv']['high'])#np.array(self.currentStage ['hsv']['high'])
                                  hsv_gaussian = cv2.GaussianBlur(hsv, (3, 3), 0)                          
                                  Lmask=cv2.inRange(hsv_gaussian,low,high)       
                                  Lmask = cv2.dilate(Lmask, np.ones((3, 3), np.uint8), iterations=2)
                                  Lmask = cv2.erode(Lmask, None, iterations=2)
                                  Lmask = cv2.medianBlur(Lmask,3)
                                  cv2.imwrite("/home/wenjing/leftView.png",Lmask)  
                                  print(self.positionSensors[18].getValue())                        
                                  if len(np.where(Lmask == 255)[0]):  
                                      leftValue = 120- min(np.where(Lmask==255)[0])
                                      print(leftValue)
                                      break
                                  else:
                                      leftValue = 1 
                                      break
                          print("左边距离:%.3f"%leftValue) 
    
                          print("机器人脑袋右转")    
                          self.mGaitManager.setXAmplitude(0.0) #为什么有的时候不太灵敏呢？              
                          self.motors[18].setPosition(math.radians(-90))
                          self.mGaitManager.step(self.mTimeStep)
                          self.myStep()                          
                          #机器人摆正
                          self.StraightenRobot()
                          self.mGaitManager.step(self.mTimeStep)
                          self.myStep()
                          while True:
                              self.mGaitManager.step(self.mTimeStep)
                              self.myStep() 
                              print(self.positionSensors[18].getValue())
                              if (np.abs(self.positionSensors[18].getValue()+math.radians(90))<0.1):
                                  image1 =sensorData.ImageGet(self.mCamera, self.mTimeStep)   #获取图片
                                  hsv = cv2.cvtColor(image1,cv2.COLOR_BGR2HSV)  #转化为hsv
                                  low = np.array(self.materialInfo ['hsv']['low'])#np.array(self.currentStage ['hsv']['low'])
                                  high =np.array(self.materialInfo ['hsv']['high'])#np.array(self.currentStage ['hsv']['high'])
                                  hsv_gaussian = cv2.GaussianBlur(hsv, (3, 3), 0)                          
                                  Rmask=cv2.inRange(hsv_gaussian,low,high)       
                                  Rmask = cv2.dilate(Rmask, np.ones((3, 3), np.uint8), iterations=2)
                                  Rmask = cv2.erode(Rmask, None, iterations=2)
                                  Rmask = cv2.medianBlur(Rmask,3)
                                  cv2.imwrite("/home/wenjing/rightView.png",Rmask)
                                  print(math.radians(self.positionSensors[18].getValue())) 
                                  if len(np.where(Rmask == 255)[0]):  
                                      rightValue =120 - min(np.where(Rmask==255)[0])
                                  else:
                                      rightValue = 1  
                                  print("右边距离:%.3f"%rightValue) 
                                  break
                                  
                          ratio = leftValue / (leftValue + rightValue) 
                          print("ratio:%.3f"%ratio)
                          #视角回正                       
                          self.motors[18].setPosition(math.radians(0))
                          self.mGaitManager.step(self.mTimeStep)
                          self.myStep() 
                          while True:
                              self.mGaitManager.step(self.mTimeStep)
                              self.myStep() 
                              if np.abs(self.positionSensors[18].getValue())<0.05:
                                  print("回正中")
                                  break
                          self.StraightenRobot()
                          self.mGaitManager.step(self.mTimeStep)
                          self.myStep() 
                          print("-----开始决策-----")   
                          if ratio < 0.3:
                              print("左侧距离过小,右转!")
                              turnFlag = "right"
                          elif ratio > 0.6:
                              print("右侧距离过小,左转!")
                              turnFlag = "left"
                          elif 7*self.mCamera.getWidth()/16 < mine_use[0][0] < 9*self.mCamera.getWidth()/16 and (turnLeft < turnRight):
                              turnFlag = "left"
                          elif 7*self.mCamera.getWidth()/16 < mine_use[0][0] < 9*self.mCamera.getWidth()/16 and (turnLeft > turnRight):
                              turnFlag = "right"
                          elif  mine_use[0][0] > self.mCamera.getWidth()/2:
                              turnFlag = "left"
                          elif mine_use[0][0] <= self.mCamera.getWidth()/2:
                              turnFlag = "right"                                
                          if turnFlag == "right":
                              turnRight += 1
                              Oblique  = -1
                          elif turnFlag == "left":
                              turnLeft += 1 
                              Oblique = -1
                print("开始转向!")
                print(Oblique)
                if (Oblique==-1):                              
                   if len(mine_use):    
                        if turnFlag=="left": 
                                print("开始左转")  
                                if self.angle[-1]>90 :
                                    print("左转过度，开始右转")
                                    turnFlag =="right"
                                change_speed = 0.7
                                self.vx = 0.0
                                self.MotionSet(v_x = 0.,v_a=change_speed)
                                self.mGaitManager.step(self.mTimeStep)
                                self.myStep()
                        if turnFlag=="right":   
                                print("开始右转") 
                                if self.angle[-1]<-90 :
                                    print("右转过度，开始左转")
                                    turnFlag =="right"
                                turnRight += 1
                                change_speed = -0.7
                                self.vx = 0.0
                                self.MotionSet(v_x = 0.,v_a=change_speed)
                                self.mGaitManager.step(self.mTimeStep)
                                self.myStep()                            
                   elif len(mine_use)==0:
                         if turnRight != turnLeft:
                                  step = 0
                                  Oblique = 0
                #视野中没有地雷，正常行走
                if len(mine_use)==0 :
                        print("没有地雷")
                        print("turnleft:%.3f"%turnLeft)
                        print("turnRight:%.3f"%turnRight)
                        print(Oblique)
                        if turnLeft == turnRight : #此时为直线行走
                            print("直线行走")
                            self.StraightenRobot()   
                            self.mGaitManager.step(self.mTimeStep)
                            self.myStep()
                        elif turnLeft != turnRight and Oblique == 0 :
                            step += 1   #斜走最多100步
                            print("step:%.3f"%step)                              

                            if step < 250:
                                self.vx = 1.0
                                self.MotionSet(v_x = 1.,v_a=0.)  
                                self.mGaitManager.setXAmplitude(1.0)
                                self.mGaitManager.step(self.mTimeStep)
                                self.myStep()
                            
                            else :
                                print("---转弯----")
                                change_speed = 0.2 * (self.angle[-1])
                                print("角度:%.3f"%self.angle[-1])
                                change_speed =  rangeLimit(change_speed, 1, -1)
                                self.MotionSet(v_x=1.0,v_a=change_speed) 
                                self.mGaitManager.setXAmplitude(change_speed)
                                self.mGaitManager.step(self.mTimeStep)
                                self.myStep()
                                if (np.abs(self.angle[-1])<0.05):
                                    turnLeft = turnRight
                    #下一关为障碍物
                        if self.isObstacle:  #self.onstacleTrue()  这里要判断下一关是不是障碍物
                            x, y, flag = self.ObstacleDetect(img)
                            if y > 40:
                                print("地雷关结束")
                                self.stageLeft.remove(2)
                                self.stage3()
                                self.isObstacle= False
                                break  
                        elif self.materialInfo['material'] == 'yellow_brick':
                            if self.passBrick(img):
                                self.stageLeft.remove(2)
                                break
                        else:
                            hsv = cv2.cvtColor(img,cv2.COLOR_BGR2HSV)
                            low = self.materialInfo['hsv']['low']
                            high = self.materialInfo['hsv']['high']
                            mask=cv2.inRange(hsv[self.mCamera.getHeight()//2:,:],np.array(low),np.array(high))
                            road = np.where(mask==255)[0]
                            num = len(road)
                            if num < 250:
                                self.stageLeft.remove(2)
                                break                               
                        if Flag == True :
                                    print("矫正!")
                                    change_speed = -0.2 * (self.angle[-1])
                                    print("角度:%.3f"%self.angle[-1])
                                    change_speed =  rangeLimit(change_speed, 1, -1)
                                    self.MotionSet(v_x=1,v_a=change_speed) 
                                    self.mGaitManager.setXAmplitude(change_speed)
                                    self.mGaitManager.step(self.mTimeStep)
                                    self.myStep()                                          
                              
    def stage3(self):
        print("----翻越障碍开始!----")  
        self.mGaitManager.setXAmplitude(1.0)  # 设置机器人前进 
        #self.turnObstacle() 
        self.StraightenRobot()
        Start = False    
        self.motors[19].setPosition(-0.3)
        #self.motors[12].setPosition(1.4)
        self.mGaitManager.step(self.mTimeStep)  
        self.myStep()
        time = count(start=0,step=1)
        print("寻找障碍物")        
        for n in time:
            self.StraightenRobot()
            self.mGaitManager.step(self.mTimeStep)
            self.myStep() 
            if n%5 ==0 and np.abs(self.positionSensors[19].getValue() + 0.3) < 0.05 and np.abs(self.angle[-1]) < 5 :
                image1 =sensorData.ImageGet(self.mCamera, self.mTimeStep)   #获取图片
                hsv = cv2.cvtColor(image1,cv2.COLOR_BGR2HSV)  #转化为hsv
                low = np.array([110,250,50]) #np.array(self.currentStage ['hsv']['low'])
                high = np.array([130,255,255]) 
                mask=cv2.inRange(hsv,low,high)                  
                mask = cv2.dilate(mask, np.ones((3, 3), np.uint8), iterations=2)
                mask = cv2.erode(mask, None, iterations=2) 
                contours,_ = cv2.findContours(mask,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE) 
                if len(contours):
                    #最大蓝色轮廓
                    area = max(contours, key=cv2.contourArea)
                    #寻找最小外接矩形并提取上面两个点坐标的中心点
                    if cv2.contourArea(area):
                        rect = cv2.minAreaRect(area)
                        box = cv2.boxPoints(rect)
                        box = np.int0(box)
                        obstacle_x = (box[2][0]+box[3][0])/2
                        obstacle_y = (box[2][1]+box[3][1])/2
                    else:
                        obstacle_x = -1
                        obstacle_y = -1
                else:
                        obstacle_x = -1
                        obstacle_y = -1    
                #当最高点的y
                if obstacle_y > 0:
                    self.mGaitManager.setXAmplitude(0.5)
                    self.mGaitManager.step(self.mTimeStep)
                    self.myStep() 
                if obstacle_y > 2*self.mCamera.getHeight()/3:
                    self.mGaitManager.setXAmplitude(0)
                    self.mGaitManager.step(self.mTimeStep)
                    self.myStep()                     
                    print("距离恰当")
                    Start = True
                    break
            if Start:
                break 
        #机器人倒转)
        #左右调整机器人位置
        self.myStep()
        time = count(start=0,step=1)    
        self.motors[19].setPosition(-0.2)
        self.motors[18].setPosition(math.radians(60))
        time = count(start=0,step=1)     
        for n in time:
            self.mGaitManager.step(self.mTimeStep)
            self.myStep() 
            #   print(self.positionSensors[18].getValue())
            if n%5==0 and (np.abs(self.positionSensors[18].getValue()-math.radians(60))<0.1):
                image1 =sensorData.ImageGet(self.mCamera, self.mTimeStep)   #获取图片
                hsv = cv2.cvtColor(image1,cv2.COLOR_BGR2HSV)  #转化为hsv
                low = np.array([110,250,50])#np.array(self.currentStage ['hsv']['low'])
                high = np.array([130,255,255])#np.array(self.currentStage ['hsv']['high'])
                hsv_gaussian = cv2.GaussianBlur(hsv, (3, 3), 0)                          
                mask=cv2.inRange(hsv_gaussian,low,high)       
                mask = cv2.dilate(mask, np.ones((3, 3), np.uint8), iterations=2)
                mask = cv2.erode(mask, None, iterations=2)
                mask = cv2.medianBlur(mask,3)
                cv2.imwrite("/home/wenjing/View.png",mask)  
                # print(self.positionSensors[18].getValue())                        
                if len(np.where(mask == 255)[0]):  
                    area = np.where(mask==255)
                    y_min = area[0].min()
                    print(y_min)
                # print(leftValue)
                while True:                                     
                    self.MotionSet(v_x = 0., v_y =0.)
                    self.StraightenRobot() 
                    self.mGaitManager.step(self.mTimeStep)  
                    self.myStep()          
                    if np.abs(self.angle[-1])>0.05:  
                        break                   
                if y_min > 20:
                    self.MotionSet(v_x = 0., v_y = -1.)
                    self.StraightenRobot() 
                    self.mGaitManager.step(self.mTimeStep)  
                    self.myStep()  
                elif y_min <= 20:
                    break
        self.motors[18].setPosition(math.radians(-60))
        time = count(start=0,step=1)     
        for n in time:
            self.mGaitManager.step(self.mTimeStep)
            self.myStep() 
            #   print(self.positionSensors[18].getValue())
            if n%5 == 0 and (np.abs(self.positionSensors[18].getValue()+math.radians(60))<0.1):
                image1 =sensorData.ImageGet(self.mCamera, self.mTimeStep)   #获取图片
                hsv = cv2.cvtColor(image1,cv2.COLOR_BGR2HSV)  #转化为hsv
                low = np.array([110,250,50])#np.array(self.currentStage ['hsv']['low'])
                high = np.array([130,255,255])#np.array(self.currentStage ['hsv']['high'])
                hsv_gaussian = cv2.GaussianBlur(hsv, (3, 3), 0)                          
                mask=cv2.inRange(hsv_gaussian,low,high)       
                mask = cv2.dilate(mask, np.ones((3, 3), np.uint8), iterations=2)
                mask = cv2.erode(mask, None, iterations=2)
                mask = cv2.medianBlur(mask,3)
                cv2.imwrite("/home/wenjing/View.png",mask)  
                # print(self.positionSensors[18].getValue()) 
                if len(np.where(mask == 255)[0]):  
                    area = np.where(mask==255)
                    y_min = area[0].min()
                    print(y_min)
                # print(leftValue)  
                while True:                                     
                    self.MotionSet(v_x = 0., v_y =0.)
                    self.StraightenRobot() 
                    self.mGaitManager.step(self.mTimeStep)  
                    self.myStep()          
                    if np.abs(self.angle[-1])>0.05:  
                        break                 
                if y_min > 20 :
                    self.MotionSet(v_x = 0., v_y = 1.)
                    self.StraightenRobot()
                    self.mGaitManager.step(self.mTimeStep)  
                    self.myStep()  
                elif y_min <= 20 :
                    break
            
        print("开始跨越")
        # 侧身九十度
        while np.abs(self.angle[-1]+90) > 5:
            u = -0.02 * (self.angle[-1]+90)
            u = np.clip(u, -1, 1)
            self.MotionSet(v_a=u)
            self.mGaitManager.step(self.mTimeStep)
            self.myStep()
        
        self.mGaitManager.stop()
        self.wait(200)

        FaceMotion = Motion('motion/Face.motion')
        FaceMotion.setLoop(False)
        for i in range(3):
            FaceMotion.play()
            while not FaceMotion.isOver():
                self.myStep()
            self.wait(200)
        
        # 跨越障碍物
        self.mMotionManager.playPage(9)
        StartMotion = Motion('./motion/start.motion')
        StartMotion.setLoop(False)
        StartMotion.play()
        while not StartMotion.isOver():
            self.myStep()
        self.angle[-1] -= 5
        self.mGaitManager.start()
        self.wait(200)
        time = repeat(0,50)
        for n in time:
          self.MotionSet(v_x=0.5)
          self.mGaitManager.step(self.mTimeStep)
          self.myStep()
             
        self.motors[19].setPosition(-0.2)
        self.StraightenRobot()
        print("----翻越障碍结束-----")     

                         
                
#我也不知道怎么回事，把它变成了奇形种式过门
    def stage4(self):
        print("----窄门关----")
        # #原思路:识别一侧柱子，然后平移，直到可以通过（这个机器人真是个胖子)，决定侧着走
        # #机器人向右侧走，就可以防止黄色地砖时与楼梯底部重合
        # #机器人右转90度
        self.motors[19].setPosition(-0.2)
        self.MotionSet(v_x = 0.0)
        self.mGaitManager.step(self.mTimeStep)
        self.myStep()  
        IsShow = False
        HasShow = False
        isTurn = False
        time = count(start=0,step=1) 
        # for n in time:
                # change_speed = -1024/32000 * (self.angle[2]+90)
                # change_speed =rangeLimit(change_speed, 1,-1)
                # self.MotionSet(v_x = 1.,v_a=change_speed)
                # self.mGaitManager.step(self.mTimeStep)
                # self.myStep()
                # print("第一次右转角度:%.3f",self.angle[-1])
                # if np.abs(np.abs(self.angle[-1])-90)<0.05: 
                    # print("右转结束")               
                    # break
        # #右转后以90度直行
        for n in time:
            self.mGaitManager.step(self.mTimeStep)
            self.myStep()
            if n%10 == 0 and  np.abs(self.positionSensors[19].getValue()+0.2)<0.05:
                change_speed = -1024/32000 * (self.angle[2]+90)
                change_speed =rangeLimit(change_speed, 1,-1)
                self.MotionSet(v_x = 1.,v_a=change_speed)
                self.mGaitManager.step(self.mTimeStep)
                self.myStep()   
                image1 =sensorData.ImageGet(self.mCamera, self.mTimeStep)   #获取图片
                hsv = cv2.cvtColor(image1,cv2.COLOR_BGR2HSV)  #转化为hsv
                low = np.array([17,4,160])  
                high = np.array([54,20,210])
                image_half = hsv[self.mCamera.getHeight()//2:,:]                   
                mask=cv2.inRange(image_half,low,high) 
                cv2.imwrite('/home/wenjing/Door.png', mask)  
                road = np.where(mask==255)[0]
                num = len(road)                
                if num < 200 :
                    print("该第二次转弯了!")
                    isTurn = True	  
                    self.MotionSet(v_x =0.) 
                    self.mGaitManager.step(self.mTimeStep)
                    self.myStep()    
                    break
        self.MotionSet(v_x =0.) 
        self.mGaitManager.step(self.mTimeStep)
        self.myStep()
        time = count(start=0,step=1) 
        # 直行
        number = 0
        self.motors[19].setPosition(-0.3)
        step = 0
        door = []
        for n in time :
            self.StraightenRobot()            
            self.mGaitManager.step(self.mTimeStep)  
            self.myStep()                
            print("开始直行")
            if n%5 == 0 and np.abs(self.positionSensors[19].getValue()+0.3)<0.05 :
                self.MotionSet(v_x = 1., v_y = 0.)
                self.mGaitManager.step(self.mTimeStep)  
                self.myStep()   
                img =sensorData.ImageGet(self.mCamera, self.mTimeStep)   #获取图片
                img_gray= cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)  #灰度化
                cv2.imwrite("/home/wenjing/bd1.png",img_gray)
                ret, binary_door = cv2.threshold(img_gray, 10, 255, cv2.THRESH_BINARY_INV)
                cv2.imwrite("/home/wenjing/bd.png",binary_door)
                contours, h = cv2.findContours(binary_door, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)  
                number = 0
                for contour in contours:
                    if cv2.contourArea(contour) > 50: 
                        number = number+1
                        break
                print(number)
                if number:
                    break
        #调整阶段
        print("开始调整角度")
        time = count(start=0,step=1) 
        step = 0
        for n in time :  
            change_speed = -1024/32000  * (self.angle[-1]-45)
            change_speed =rangeLimit(change_speed, 1,-1)
            self.MotionSet(v_x = 0.,v_a=change_speed)
            self.mGaitManager.step(self.mTimeStep)
            self.myStep()  
            if np.abs(self.angle[-1]-45)<10:
                print("调整完毕!")
                break
        time = count(start=0,step=1) 
        step = 0
        for n in time :     
            change_speed = -1024/32000  * (self.angle[-1]-45)
            change_speed =rangeLimit(change_speed, 1,-1)
            self.mGaitManager.step(self.mTimeStep)
            self.myStep()  
            if n%5 == 0 and  np.abs(self.angle[-1] - 45) < 10:  
                self.MotionSet(v_x = 1.,v_a=change_speed)
                self.mGaitManager.step(self.mTimeStep)
                self.myStep()             
                img =sensorData.ImageGet(self.mCamera, self.mTimeStep)   #获取图片
                img_gray= cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)  #灰度化
                ret, binary_door = cv2.threshold(img_gray, 10, 255, cv2.THRESH_BINARY_INV)
                cv2.imwrite("/home/wenjing/bd2.png",binary_door)
                contours, h = cv2.findContours(binary_door, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE) 
                if len(contours):
                    contours = sorted(contours,key=lambda cnt : tuple(cnt[cnt[:,:,0].argmin()][0])[0])
                    cnt= contours[0] 
                    bottommost=tuple(cnt[cnt[:,:,1].argmax()][0])
                    print("bottommost:%.3f"%bottommost[1])
                    if bottommost[1] > 30 :
                        print("左行结束！")
                        break
        print("第二次角度调整!")
        for n in time :  
            change_speed = -1024/32000  * (self.angle[-1]+70)
            change_speed =rangeLimit(change_speed, 1,-1)
            self.MotionSet(v_x = 0.,v_a=change_speed)
            self.mGaitManager.step(self.mTimeStep)
            self.myStep()  
            if np.abs(self.angle[-1]+70)<10:
                print("调整完毕!")
                break 
        print("第二次斜走!")       
        time = count(start=0,step=1)                         
        for n in time :     
            change_speed = -1024/32000  * (self.angle[-1]+70)
            change_speed =rangeLimit(change_speed, 1,-1)
            self.MotionSet(v_x = 0.1,v_y  = 4.,v_a=change_speed)
            self.mGaitManager.step(self.mTimeStep)
            self.myStep()  
            if n%5 == 0 and  np.abs(self.angle[-1] + 70) < 10:             
                img =sensorData.ImageGet(self.mCamera, self.mTimeStep)   #获取图片
                img_gray= cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)  #灰度化
                cv2.imwrite("/home/wenjing/bd1.png",img_gray)
                ret, binary_door = cv2.threshold(img_gray, 10, 255, cv2.THRESH_BINARY_INV)
                cv2.imwrite("/home/wenjing/bd.png",binary_door)
                contours, h = cv2.findContours(binary_door, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
                num = 0  
                if len(contours):
                    contours = sorted(contours,key=lambda cnt : tuple(cnt[cnt[:,:,0].argmin()][0])[0])
                    cnt= contours[0] 
                    bottommost=tuple(cnt[cnt[:,:,1].argmax()][0])
                    print("bottommost_right:%.3f"%bottommost[1])                    
                    if bottommost[1] > 80 :
                        print("右行结束！")
                        break
                else:
                    if n>=500:
                        print(n)
                        break
            self.mGaitManager.step(self.mTimeStep)
            self.myStep()   
        # print("居然要走三次？！")
        # print("第三次角度调整!")
        # for n in time :  
            # change_speed = -1024/32000  * (self.angle[-1]-45)
            # change_speed =rangeLimit(change_speed, 1,-1)
            # self.MotionSet(v_x = 0.,v_a=change_speed)
            # self.mGaitManager.step(self.mTimeStep)
            # self.myStep()  
            # if np.abs(self.angle[-1]-45)<10:
                # print("调整完毕!")
                # break 
        # print("第三次斜走!")       
        # time = count(start=0,step=1)                         
        # for n in time :     
            # change_speed = -1024/32000  * (self.angle[-1]-45)
            # change_speed =rangeLimit(change_speed, 1,-1)
            # self.MotionSet(v_x = 0.,v_a=change_speed)
            # self.mGaitManager.step(self.mTimeStep)
            # self.myStep()  
            # if n%5 == 0 and  np.abs(self.angle[-1] - 45) < 10:             
                # img =sensorData.ImageGet(self.mCamera, self.mTimeStep)   #获取图片
                # img_gray= cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)  #灰度化
                # cv2.imwrite("/home/wenjing/bd1.png",img_gray)
                # ret, binary_door = cv2.threshold(img_gray, 10, 255, cv2.THRESH_BINARY_INV)
                # cv2.imwrite("/home/wenjing/bd.png",binary_door)
                # contours, h = cv2.findContours(binary_door, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
                # num = 0  
                # if len(contours):
                    # contours = sorted(contours,key=lambda cnt : tuple(cnt[cnt[:,:,0].argmin()][0])[0])
                    # cnt= contours[0] 
                    # bottommost=tuple(cnt[cnt[:,:,1].argmax()][0])
                    # print("bottommost_right:%.3f"%bottommost[1])                    
                    # if bottommost[1] > 60 or n >=170:
                        # print("左行结束！")
                        # break
            # self.mGaitManager.step(self.mTimeStep)
            # self.myStep()                      
                # if num==0:
                    # IsShow ==False
                # if IsShow ==False and HasShow ==False:
                    # step = 0
                    # print("即将开始回正")
                    # if step <=30:
                        # step = step+1
                    # else:
                        # break    
                    # #求黑色部分的质心
                        # M = cv2.moments(contour)  
                        # center_x = int(M["m10"] / M["m00"])
                        # center_y = int(M["m01"] / M["m00"])
                        # door.append((center_x,center_y)) 
        print("开始回正")
        time = count(start=0,step=1) 
        step = 0
        for n in time :     
            change_speed = -1024/32000  * (self.angle[-1])
            change_speed =rangeLimit(change_speed, 1,-1)
            self.MotionSet(v_x = 0.,v_a=change_speed)
            self.mGaitManager.step(self.mTimeStep)
            self.myStep()            
            if np.abs(self.angle[-1])<5:
                print("调整完成")
                break
        # print("回归中间")
        # time = count(start=0,step=1) 
        # step = 0
        # for n in time :  
            # self.mGaitManager.step(self.mTimeStep)
            # self.myStep()
            # if np.abs(self.angle[-1])<5 and step <=50:   
                 # self.MotionSet(v_x = 0.,v_y=1., v_a = 0)
                 # step = step+1
                 # self.mGaitManager.step(self.mTimeStep)
                 # self.myStep()            
            # if step > 50:
                # break                  
                # right_one =sorted(door,key = lambda x:x[1])[-1]
                # print(right_one[0])
                # if (not (16*self.mCamera.getWidth()/16 > right_one[0] >14*self.mCamera.getWidth()/16)) and (step < 160) :
                      # print("横向调整")
                      # self.MotionSet(v_x = 0., v_y = 1.) 
                      # self.StraightenRobot()             
                      # self.mGaitManager.step(self.mTimeStep)  
                      # self.myStep() 
                      # step = step+1
                      # print(step)
                # else:
                      # print("调整完成")
                      # break   
 
               #结束关卡
               #if 下一关是楼梯 且 这一关为黄色地砖  ：
                #  判断红色     
                #否则，正常结束                           
        time = count(start=0,step=1) 
        for n in time:                
            self.StraightenRobot()
            self.MotionSet(v_x = 1.)             
            self.mGaitManager.step(self.mTimeStep)  
            self.myStep()  
            image1 =sensorData.ImageGet(self.mCamera, self.mTimeStep)   #获取图片

            cv2.imwrite('/home/wenjing/door_out.png', mask)
            if self.isObstacle:
                    x,y ,flag= self.ObstacleDetect(image1)
                    if y > 20:
                        #print(ob_y)
                        print("宅门关结束!")
                        self.stageLeft.remove(4)
                        self.stage3()
                        self.isObstacle = False
                        break
                # 若本关后无障碍物，则正常结束
            elif self.materialInfo['material'] == 'yellow_brick':
                    if self.passBrick(image1):
                        self.stageLeft.remove(4) 
                        break
            else:           
                hsv = cv2.cvtColor(image1,cv2.COLOR_BGR2HSV)  #转化为hsv
                low =np.array(self.materialInfo ['hsv']['low'])
                high = np.array(self.materialInfo ['hsv']['low'])                        
                mask=cv2.inRange(hsv,low,high)
                road = np.where(mask==255)[0]
                num = len(road)  
                print(num)            
                if num < 200 :
                    print("---窄门关结束！----")
                    self.stageLeft.remove(4)
                    break
        self.MotionSet(v_x = 0.,v_y=0)             
        self.mGaitManager.step(self.mTimeStep)  
        self.myStep()                         

    def stage5(self):
        print("---绿色独木桥开始----")
        self.motors[19].setPosition(0)
        self.vx = 0.0
        self.MotionSet(v_x = 0.,v_a=0.)  
        self.mGaitManager.step(self.mTimeStep)  # 步态生成器生成一个步长的动作  
        self.myStep() 
        time = count(start=0,step=1)
        Value = 0
        for n in time: 
        #识别绿色独木桥  
            self.mGaitManager.step(self.mTimeStep)  # 步态生成器生成一个步长的动作  
            self.myStep()  
            if n % 10 == 0 and np.abs(self.positionSensors[19].getValue())<0.05:                       
                image1 =sensorData.ImageGet(self.mCamera, self.mTimeStep)   #获取图片
                hsv = cv2.cvtColor(image1,cv2.COLOR_BGR2HSV)  #转化为hsv
                low = np.array([50,60 ,50])#np.array(self.currentStage ['hsv']['low'])
                high = np.array([90,245,255])#np.array(self.currentStage ['hsv']['high'])
                hsv_gaussian = cv2.GaussianBlur(hsv, (3, 3), 0)                          
                mask=cv2.inRange(hsv_gaussian,low,high)       
                mask = cv2.dilate(mask, np.ones((3, 3), np.uint8), iterations=2)
                mask = cv2.erode(mask, None, iterations=2)
                mask = cv2.medianBlur(mask,3)
                bridge = 0
                cv2.imwrite("/home/wenjing/bridge.png",mask)
                #原方案直接计算左右距离,不稳定,修改为求质心
                #求绿色块的质心与面积
               # print("面积与质心")
                counters = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[-2]
                if len(counters):
                            area = max(counters, key=cv2.contourArea)  
                            bridge = cv2.contourArea(area)
                            #计算质心
                            if bridge > 0:
                                M = cv2.moments(area)  
                                center_x = int(M["m10"] / M["m00"])
                                center_y = int(M["m01"] / M["m00"])
                                mid_point = [center_x,center_y]                     
                else:
                    mid_point = [-1,-1]
                # print(mid_point)
                # print(cv2.contourArea(area))
                if 16000 >bridge>800:
                    if mid_point[0] > 3*self.mCamera.getWidth()/5:
                       print("----过于偏左----")
                       self.MotionSet(v_x = 0,v_y=-1.0)   
                       self.mGaitManager.step(self.mTimeStep)  # 步态生成器生成一个步长的动作  
                       self.myStep()                   
                    elif mid_point[0] <2*self.mCamera.getWidth()/5:  
                        print("----过于偏右-----") 
                        self.MotionSet(v_x = 0.,v_y =1.0)   
                        self.mGaitManager.step(self.mTimeStep)  # 步态生成器生成一个步长的动作  
                        self.myStep() 
                    else:
                        print("距离正好")
                        self.MotionSet(v_x = 1.5,v_y = 0.) 
                        self.StraightenRobot() 
                        self.mGaitManager.step(self.mTimeStep)  # 步态生成器生成一个步长的动作  
                        self.myStep() 
                else:
                     self.MotionSet(v_x = 1.0,v_y = 0.) 
                     self.StraightenRobot() 
                     self.mGaitManager.step(self.mTimeStep)  # 步态生成器生成一个步长的动作  
                     self.myStep() 
                     num = len(np.where(mask==255)[0])
                     if num <100 or num > 3000:
                         break
            #除了最后一关,前进
        if len(self.stageLeft)> 2:
            time = repeat(0,200)
            for n in time:
                self.MotionSet(v_x = 1.,v_y =0.0)  
                self.StraightenRobot()
                self.mGaitManager.step(self.mTimeStep)  # 步态生成器生成一个步长的动作
                self.myStep()  # 仿真一个步长
        self.MotionSet(v_x = 0.,v_y =0.0) 
        self.StraightenRobot()
        self.stageLeft.remove(5)
        print("-------绿色独木桥结束------")   
    
    def whereAmI(self,img):
        # 判定当前我在哪种花色的地面上
        hsv = cv2.cvtColor(img,cv2.COLOR_BGR2HSV)
        maxNum = 0
        currentInfo = {}
        for info in raceMaterialInfo:
            low = info['hsv']['low']
            high = info['hsv']['high']
            mask=cv2.inRange(hsv,np.array(low),np.array(high))
            road = np.where(mask==255)[0]
            num = len(road)
            if num > maxNum:
                currentInfo = info
                maxNum = num
        return currentInfo
    
    def stage6(self):
        print("--踢球进洞开始了--")
        self.MotionSet(v_x = 0.,v_y =0.0)    
        # 先上下抬头，同时找到球和洞
        self.motors[19].setPosition(0.5)  
        while not np.abs(self.positionSensors[19].getValue()-0.5) < 0.05:
            change_speed= -0.02 * (self.angle[-1])
            change_speed = np.clip(change_speed, -1, 1)
            self.MotionSet(v_a=change_speed)
            self.mGaitManager.step(self.mTimeStep)
            self.myStep()
        backwardFlag = False
        self.motors[19].setPosition(0.35) 
        time=count(start=0,step=1)
        for n in time:
            change_speed = -0.02 * (self.angle[-1])
            change_speed = np.clip(change_speed, -1, 1)
            x = -1. if backwardFlag else 0.
            self.MotionSet(v_x =x ,v_a=change_speed)
            self.mGaitManager.step(self.mTimeStep)
            self.myStep()
            image1 =sensorData.ImageGet(self.mCamera, self.mTimeStep)   #获取图片
            image1 = cv2.copyMakeBorder(image1,1,1,1,1,cv2.BORDER_CONSTANT,value=[0,0,0])
            MaterialInfo = self.whereAmI(image1)
            self.materialInfo = MaterialInfo
            low =  self.materialInfo['hsv']['low']
            high = self.materialInfo['hsv']['high']
            hsv = cv2.cvtColor(image1,cv2.COLOR_BGR2HSV)
            mask=cv2.inRange(hsv,np.array(low),np.array(high))
            kernel = cv2.getStructuringElement(cv2.MORPH_RECT,(3,3))
            mask = cv2.morphologyEx(mask, cv2.MORPH_CLOSE, kernel,iterations=3)
            contours, hierarchy = cv2.findContours(mask,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
            image1= cv2.bitwise_and( image1, image1,mask=mask)
            # 蒙版之后开始找球
            hsv = cv2.cvtColor( image1, cv2.COLOR_BGR2HSV)
            low = [105,120,35] #球和洞的hsv
            high = [170,255,255]
            mask = cv2.inRange(hsv, np.array(low), np.array(high))
            kernel = cv2.getStructuringElement(cv2.MORPH_RECT,(3,3))
            mask = cv2.morphologyEx(mask, cv2.MORPH_CLOSE, kernel,iterations=3) # 闭运算：先膨胀后腐蚀，用来连接被误分为许多小块的对象
            contours,_ = cv2.findContours(mask,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
            ballHoleInfo = []
            for contour in contours:
                area = cv2.contourArea(contour)
                x,y,w,h = cv2.boundingRect(contour)
                if 10 < area < 400 and 0.25*w < h < 4*w and y>10: 
                    print("--12--")        
                    ballHoleInfo.append({'center':(int(x+w/2),int(y+w/2)),'area':w*h})
            if len(ballHoleInfo) == 2:
                print('同时检测到球和洞时头部位置 ')
                break #检测到球和洞
                
        if np.abs(self.positionSensors[19].getValue()-0.35) < 0.05:
                backwardFlag = True    
        
        # 判定左右关系
        ballHoleInfo = sorted(ballHoleInfo,key= lambda x:x['area'])
        if ballHoleInfo[0]['center'][0] < ballHoleInfo[1]['center'][0]:
            case = '洞在右侧'
            print('洞在右侧')
        else:
            case = '洞在左侧'
            print('洞在左侧')

        # 若太远，先走一段。不要在上一关结束的地方直接转。
        if ballHoleInfo[0]['center'][1] < 70:
            print('可以先走一小段')
            ns = repeat(0,(400-ballHoleInfo[1]['center'][1]))
            for n in ns:
                change_speed = -0.02 * (self.angle[-1])
                change_speed = np.clip(change_speed, -1, 1)
                self.MotionSet(v_x=1.0,v_a=change_speed)
                self.mGaitManager.step(self.mTimeStep)  # 步态生成器生成一个步长的动作
                self.myStep()  # 仿真一个步长
        else:
            print('不可以先走一段')
            ns = repeat(0,100)
            for n in ns:
                change_speed = -0.02 * (self.angle[-1])
                change_speed = np.clip(change_speed, -1, 1)
                self.MotionSet(v_x=0.0,v_a=change_speed)
                self.mGaitManager.step(self.mTimeStep)  # 步态生成器生成一个步长的动作
                self.myStep()  # 仿真一个步长
        
        if case == '洞在右侧':
            # 向左转向90
            self.motors[19].setPosition(-0.15)
            while np.abs(self.angle[-1]-90) > 20:
                change_speed = -0.02 * (self.angle[-1]-90)
                change_speed = np.clip(change_speed, -1, 1)
                self.MotionSet(v_a=change_speed)
                self.mGaitManager.step(self.mTimeStep)
                self.myStep()
            # 走到道路边缘
            time = count(start=0,step=1) 
            for n in time:
                changespeed = -0.02 * (self.angle[-1]-90)
                changespeed = np.clip(changespeed, -1, 1)
                self.MotionSet(v_x=1.,v_a=changespeed)
                self.mGaitManager.step(self.mTimeStep)
                self.myStep()
                if n % 5 == 0:
                    image1 =sensorData.ImageGet(self.mCamera, self.mTimeStep)   #获取图片
                    hsv = cv2.cvtColor(image1,cv2.COLOR_BGR2HSV)
                    low = self.materialInfo['hsv']['low']
                    high = self.materialInfo['hsv']['high']
                    mask=cv2.inRange(hsv[self.mCamera.getHeight()//2:,:],np.array(low),np.array(high))
                    road = np.where(mask==255)[0]
                    num = len(road)
                    if num < 100:
                        break

             # 回正，侧头
            self.MotionSet(v_x=0.)
            self.motors[18].setPosition(math.radians(-45))
            self.motors[19].setPosition(0.1)
            self.StraightenRobot(eps=10)

            # 向前走，直到洞和球垂直出现在视野中央
            time = count(start=0,step=1) 
            for n in time:
                change_speed = -0.02 * (self.angle[-1])
                change_speed = np.clip(change_speed, -1, 1)
                self.MotionSet(v_x=1.,v_a=change_speed)
                self.mGaitManager.step(self.mTimeStep)  # 步态生成器生成一个步长的动作
                self.myStep()  # 仿真一个步长
                if n % 5 == 0 and np.abs(self.angle[-1])<5:
                    image1 =sensorData.ImageGet(self.mCamera, self.mTimeStep)   #获取图片
                    hsv = cv2.cvtColor(image1,cv2.COLOR_BGR2HSV)
                    low = [105,160,35] #球和洞的hsv
                    high = [170,255,255]
                    mask=cv2.inRange(hsv,np.array(low),np.array(high))
                    kernel = cv2.getStructuringElement(cv2.MORPH_RECT,(3,3))
                    mask = cv2.morphologyEx(mask, cv2.MORPH_CLOSE, kernel,iterations=3) # 闭运算：先膨胀后腐蚀，用来连接被误分为许多小块的对象
                    contours,_ = cv2.findContours(mask,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
                    ballHoleInfo = []
                    for contour in contours:
                        area = cv2.contourArea(contour)
                        if area > 5:
                            x,y,w,h = cv2.boundingRect(contour)
                            ballHoleInfo.append({'center':(int(x+w/2),int(y+w/2)),'area':w*h})
                        if len(ballHoleInfo)>=1 and 75 < ballHoleInfo[0]['center'][0] < 95 :
                            print("踢球进洞了")
                        break

            # 向右转向45
            self.motors[18].setPosition(0)
            self.motors[19].setPosition(0.1)
            while np.abs(self.angle[-1]+45) > 1:
                change_speed = -0.02 * (self.angle[-1]+45)
                change_speed = np.clip(change_speed, -1, 1)
                self.MotionSet(vA=change_speed)
                self.mGaitManager.step(self.mTimeStep)
                self.myStep()
            
             # 前进，直接能把球撞进去
            time =  count(start=0,step=1) 
            bias=0.0
            forwardFlag = False
            for n in time:
                change_speed = -0.02 * (self.angle[-1]+45)
                change_speed = np.clip(change_speed, -1, 1)
                if forwardFlag:
                    self.MotionSet(v_x=1.0,v_y=0.0,v_a=change_speed)
                else:
                    self.MotionSet(v_x=0.0,v_y=0.05*bias,v_a=change_speed)
                self.mGaitManager.step(self.mTimeStep)  # 步态生成器生成一个步长的动作
                self.myStep()  # 仿真一个步长
                # 配准球洞
                if n%5 == 0:
                    image1 =sensorData.ImageGet(self.mCamera, self.mTimeStep)   #获取图片
                    hsv = cv2.cvtColor(image1,cv2.COLOR_BGR2HSV)
                    low = [105,120,35] #球和洞的hsv
                    high = [170,255,255]
                    mask=cv2.inRange(hsv,np.array(low),np.array(high))
                    kernel = cv2.getStructuringElement(cv2.MORPH_RECT,(3,3))
                    mask = cv2.morphologyEx(mask, cv2.MORPH_CLOSE, kernel,iterations=3) # 闭运算：先膨胀后腐蚀，用来连接被误分为许多小块的对象
                    contours,_ = cv2.findContours(mask,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
                    if len(contours):
                        cnt = max(contours, key=cv2.contourArea)
                        M = cv2.moments(cnt)  # 计算第一条轮廓的各阶矩,字典形式
                        try:
                            center_x = int(M["m10"] / M["m00"])
                            center_y = int(M["m01"] / M["m00"])
                            bias = 70 - center_x
                        except:
                            bias = 0
                            print('出错')
                        if -2 < bias < 2:
                            forwardFlag = True
                        if center_y > 90:
                            break
                    else:
                        bias = 0.

                    # 转身离去
            print('踢球完毕')
            self.MotionSet(v_x=0.)
            self.StraightenRobot(eps=1)

        elif case == '洞在左侧':
            # 向右转向90
            self.motors[19].setPosition(-0.15)
            while np.abs(self.angle[-1]+90) > 20:
                change_speed = -0.02 * (self.angle[-1]+90)
                change_speed = np.clip(change_speed, -1, 1)
                self.MotionSet(v_a=change_speed)
                self.mGaitManager.step(self.mTimeStep)
                self.myStep()
            # 走到道路边缘
            time = count(start=0,step=1) 
            for n in time:
                self.MotionSet(v_x = 1)    
                self.motors[18].setPosition(math.radians(90))
                self.mGaitManager.step(self.mTimeStep)
                self.myStep() 
                if n % 5 == 0 :
                    image1 =sensorData.ImageGet(self.mCamera, self.mTimeStep)   #获取图片
                    hsv = cv2.cvtColor(image1,cv2.COLOR_BGR2HSV)
                    low = self.materialInfo['hsv']['low']
                    high = self.materialInfo['hsv']['high']
                    mask=cv2.inRange(hsv[self.mCameraHeight//2:,:],np.array(low),np.array(high))
                    road = np.where(mask==255)[0]
                    num = len(road)
                    if num < 100:
                        break

             # 回正，侧头
            self.MotionSet(v_x=0.)
            self.motors[18].setPosition(math.radians(45))
            self.motors[19].setPosition(0.1)
            self.StraightenRobot()

            # 向前走，直到洞和球垂直出现在视野中央
            time = count(start=0,step=1) 
            for n in time:
                change_speed = -0.02 * (self.angle[-1])
                change_speed = np.clip(change_speed, -1, 1)
                self.MotionSet(v_x=1.,v_a=change_speed)
                self.mGaitManager.step(self.mTimeStep)  # 步态生成器生成一个步长的动作
                self.myStep()  # 仿真一个步长
                if n % 5 == 0 and np.abs(self.angle[-1])<5:
                    image1 =sensorData.ImageGet(self.mCamera, self.mTimeStep)   #获取图片
                    hsv = cv2.cvtColor(image1,cv2.COLOR_BGR2HSV)
                    low = [105,160,35] #球和洞的hsv
                    high = [170,255,255]
                    mask=cv2.inRange(hsv,np.array(low),np.array(high))
                    kernel = cv2.getStructuringElement(cv2.MORPH_RECT,(3,3))
                    mask = cv2.morphologyEx(mask, cv2.MORPH_CLOSE, kernel,iterations=3) # 闭运算：先膨胀后腐蚀，用来连接被误分为许多小块的对象
                    contours,_ = cv2.findContours(mask,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
                    ballHoleInfo = []
                    for contour in contours:
                        area = cv2.contourArea(contour)
                        if area > 5:
                            x,y,w,h = cv2.boundingRect(contour)
                            ballHoleInfo.append({'center':(int(x+w/2),int(y+w/2)),'area':w*h})
                        if len(ballHoleInfo)>=1 and 75 < ballHoleInfo[0]['center'][0] < 95 :
                            print("踢球进洞了")
                        break

            # 向右转向45
            self.motors[18].setPosition(0)
            self.motors[19].setPosition(0.1)
            while np.abs(self.angle[-1]-45) > 1:
                change_speed = -0.02 * (self.angle[-1]-45)
                change_speed = np.clip(change_speed, -1, 1)
                self.MotionSet(vA=change_speed)
                self.mGaitManager.step(self.mTimeStep)
                self.myStep()
            
             # 前进，直接能把球撞进去
            time =  count(start=0,step=1) 
            bias=0.0
            forwardFlag = False
            for n in time:
                change_speed = -0.02 * (self.angle[-1]+45)
                change_speed = np.clip(change_speed, -1, 1)
                if forwardFlag:
                    self.MotionSet(v_x=1.0,v_y=0.0,v_a=change_speed)
                else:
                    self.MotionSet(v_x=0.0,v_y=0.05*bias,v_a=change_speed)
                self.mGaitManager.step(self.mTimeStep)  # 步态生成器生成一个步长的动作
                self.myStep()  # 仿真一个步长
                # 配准球洞
                if n%5 == 0:
                    image1 =sensorData.ImageGet(self.mCamera, self.mTimeStep)   #获取图片
                    hsv = cv2.cvtColor(image1,cv2.COLOR_BGR2HSV)
                    low = [105,120,35] #球和洞的hsv
                    high = [170,255,255]
                    mask=cv2.inRange(hsv,np.array(low),np.array(high))
                    kernel = cv2.getStructuringElement(cv2.MORPH_RECT,(3,3))
                    mask = cv2.morphologyEx(mask, cv2.MORPH_CLOSE, kernel,iterations=3) # 闭运算：先膨胀后腐蚀，用来连接被误分为许多小块的对象
                    contours,_ = cv2.findContours(mask,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
                    if len(contours):
                        cnt = max(contours, key=cv2.contourArea)
                        M = cv2.moments(cnt)  # 计算第一条轮廓的各阶矩,字典形式
                        try:
                            center_x = int(M["m10"] / M["m00"])
                            center_y = int(M["m01"] / M["m00"])
                            bias = 90 - center_x
                        except:
                            bias = 0
                            print('出错')
                        if -2 < bias < 2:
                            forwardFlag = True
                        if center_y > 90:
                            break
                    else:
                        bias = 0.

                    # 转身离去
            print('踢球完毕')
            self.MotionSet(v_x=0.)
            self.StraightenRobot(eps=1)

        self.motors[19].setPosition(-0.2)
        time = count(start=0,step=1) 
        for n in time:
            change_speed = -0.02 * (self.angle[-1])
            change_speed = np.clip(change_speed, -1, 1)
            self.MotionSet(v_x=1.,v_a=change_speed)
            self.StraightenRobot()
            self.mGaitManager.step(self.mTimeStep)
            self.myStep()
            if n % 5 == 0:
                image1 =sensorData.ImageGet(self.mCamera, self.mTimeStep)   #获取图片
                # 若本关后接高障碍物，则进stage4()
                if self.IsObstacle:
                    x, y, flag = self.ObstacleDetect(image1)
                    if y > 40:
                        self.stage3()
                        self.IsObstacle= False
                # 若本关后无障碍物，则正常结束
                elif self.materialInfo['material'] == 'yello_brick':
                    if self.passBrick(image1): 
                        break
                else:
                    hsv = cv2.cvtColor(image1,cv2.COLOR_BGR2HSV)
                    low = self.materialInfo['hsv']['low']
                    high = self.materialInfo['hsv']['high']
                    mask=cv2.inRange(hsv[self.mCameraHeight//2:,:],np.array(low),np.array(high))
                    road = np.where(mask==255)[0]
                    num = len(road)
                    if num < 250: 
                        break
        print("---踢球关结束---")
        self.stageLeft.remove(6)
    
    def stage7(self):
        print("----开始爬楼梯了----")
        # 直行直到看到第一块蓝色楼梯
        self.motors[19].setPosition(-0.2)
        backwardFlag = False
        time = count(start=0,  step=1)
        for n in time:
            change_speed = -0.02 * (self.angle[-1])
            change_speed = np.clip(change_speed, -1, 1)
            if not backwardFlag:
                self.MotionSet(v_x=1.0,v_a=change_speed)
            else:
                self.MotionSet(v_x=-1.0,v_a=change_speed)
            self.mGaitManager.step(self.mTimeStep)  # 步态生成器生成一个步长的动作
            self.myStep()  # 仿真一个步长
            if n % 5 == 0 and np.abs(self.positionSensors[19].getValue()+0.2)<0.05:
                image1 =sensorData.ImageGet(self.mCamera, self.mTimeStep)   #获取图片
                hsv = cv2.cvtColor(image1, cv2.COLOR_BGR2HSV)
                low = [100,110,150] # 第一块蓝色阶梯的hsv
                high = [110,200,255]
                mask = cv2.inRange(hsv, np.array(low), np.array(high))
                cnts = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[-2]
                if len(cnts)>0:
                    cnt = max(cnts, key=cv2.contourArea)
                    bottommost=tuple(cnt[cnt[:,:,1].argmax()][0])
                    if bottommost[1] > 40 : backwardFlag = True
                    if bottommost[1] > 15 and not backwardFlag : break
                    if bottommost[1] < 15 and backwardFlag: break

        # 若太偏，先修正一下
        leftmost = tuple(cnt[cnt[:,:,0].argmin()][0])
        rightmost=tuple(cnt[cnt[:,:,0].argmax()][0])
        midX = 0.5*(leftmost[0]+rightmost[0]) # 视野中蓝色楼梯中点

        if midX < 65:
            direction = 60
        elif midX > 90:
            direction = -60
        else:
            direction = 0

        while(np.abs(self.angle[-1]-direction)>5):
            change_speed = -0.02 * (self.angle[-1]-direction)
            change_speed = np.clip(change_speed, -1, 1)
            self.MotionSet(v_x=1.0,v_a=change_speed)
            self.mGaitManager.step(self.mTimeStep)
            self.myStep()

        self.MotionSet(v_x=0.0)
        self.StraightenRobot(eps=5)

        # 配准上楼的位置
        time = count(start=0, step=1)
        for n in time:
            change_speed = -0.02 * (self.angle[-1])
            change_speed = np.clip(change_speed, -1, 1)
            self.MotionSet(v_x=1.0,v_a=change_speed)
            self.mGaitManager.step(self.mTimeStep)  # 步态生成器生成一个步长的动作
            self.myStep()  # 仿真一个步长
            if n % 5 == 0:
                # img processing
                image1 = sensorData.ImageGet(self.mCamera, self.mTimeStep)
                hsv = cv2.cvtColor(image1, cv2.COLOR_BGR2HSV)
                low = [100,110,150] # 第一块蓝色阶梯的hsv
                high = [110,200,255]
                mask = cv2.inRange(hsv, np.array(low), np.array(high))
                cnts = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[-2]
                if len(cnts):
                    cnt = max(cnts, key=cv2.contourArea)
                    bottommost=tuple(cnt[cnt[:,:,1].argmax()][0])
                    if bottommost[1] >= 9*self.mCamera.getHeight()/10:
                        print('调整结束')
                        break

            # 停下
        for i in range(50):
                change_speed = -0.02 * (self.angle[-1])
                change_speed = np.clip(change_speed, -1, 1)
                self.MotionSet(v_x=0.0,v_a=change_speed)
                self.mGaitManager.step(self.mTimeStep)  # 步态生成器生成一个步长的动作
                self.myStep()  # 仿真一个步长
               
        self.mGaitManager.stop()
        self.wait(200)

        stepMotion = Motion('motion/Check.motion')
        stepMotion.setLoop(False)
        for i in range(3):
               stepMotion.play()
               while not stepMotion.isOver():
                    self.myStep()
        stepMotion = Motion('motion/inv_check.motion')
        stepMotion.setLoop(False)
        for i in range(2):
               stepMotion.play()
               while not stepMotion.isOver():
                   self.myStep()

            # 上楼梯
        stepMotion = Motion('motion/Up.motion')
        stepMotion.setLoop(False)
        stepMotion.play()
        while not stepMotion.isOver():
               self.myStep()
            # 下楼梯
        stepMotion = Motion('motion/Down.motion')
        stepMotion.setLoop(False)
        stepMotion.play()
        while not stepMotion.isOver():
               self.myStep()
        stepMotion.play()
        while not stepMotion.isOver():
               self.myStep()
        print("台阶结束")
        stepMotion = Motion('motion/inv_check.motion')
        stepMotion.setLoop(False)
        for i in range(4):
               stepMotion.play()
               while not stepMotion.isOver():
                   self.myStep()
        self.angle = np.array([0., 0., 0.])
        # 抬头看下一关，是不是还是黄色的    
        self.motors[19].setPosition(0.6)
        time=count(start=0,step=1)
        for n in time:
            self.myStep()
            image1 = sensorData.ImageGet(self.mCamera, self.mTimeStep)
            if np.abs(self.positionSensors[19].getValue()-0.6)<0.05:
                break
        hsv = cv2.cvtColor(image1, cv2.COLOR_BGR2HSV)
        low = self.materialInfo['hsv']['low']
        high = self.materialInfo['hsv']['high']
        mask = cv2.inRange(hsv,np.array(low),np.array(high))
        road = np.where(mask==255)[0]
        farest = np.where(road<5)[0]
        self.mGaitManager.start()
        self.wait(200)
        if len(farest) > 100:
            print('有延伸的黄色路面')
            # 写死冲一段
            self.motors[19].setPosition(-0.2)
            time=count(start=0,step=1)
            for n in time:
                change_speed= -0.02 * (self.angle[-1])
                change_speed = np.clip(change_speed, -1, 1)
                self.MotionSet(v_x=1.0,v_a=change_speed)
                self.mGaitManager.step(self.mTimeStep)  # 步态生成器生成一个步长的动作
                self.myStep()  # 仿真一个步长
        else:
            # 用黄色结束来作为关卡结束的标志
            print('没有黄色路面了')
            low =  [0,180,205] # 大红
            high = [255,255,255]
            hsv = cv2.cvtColor(image1,cv2.COLOR_BGR2HSV)
            mask=cv2.inRange(hsv,np.array(low),np.array(high))
            kernel = cv2.getStructuringElement(cv2.MORPH_RECT,(1,1))
            mask = cv2.morphologyEx(mask, cv2.MORPH_OPEN, kernel,iterations=3)
            # 下最后一个红色坡,并结束这关
            self.motors[19].setPosition(-0.2)
            time=count(start=0,step=1)
            for n in time:
                change_speed = -0.02 * (self.angle[-1])
                change_speed =np.clip(change_speed, -1, 1)
                self.MotionSet(v_x=1.0,v_a=change_speed)
                self.mGaitManager.step(self.mTimeStep)  # 步态生成器生成一个步长的动作
                self.myStep()  # 仿真一个步长
                if n > 100 and n % 5 == 0 and np.abs(self.positionSensors[19].getValue()+0.2)<0.05:
                    # img processing
                    rgb_raw = sensorData.ImageGet(self.mCamera, self.mTimeStep)
                    hsv = cv2.cvtColor(rgb_raw, cv2.COLOR_BGR2HSV)
                    low = self.materialInfo['hsv']['low']
                    high = self.materialInfo['hsv']['high']
                    mask1=cv2.inRange(hsv[self.mCamera.getHeight()//2:,:],np.array(low),np.array(high)) # 黄色mask
                    low =  [0,180,205] # 大红
                    high = [255,255,255]
                    mask2=cv2.inRange(hsv[self.mCamera.getHeight()//2:,:],np.array(low),np.array(high)) # 红色mask
                    mask = cv2.bitwise_or(mask1, mask2)  #取并集
                    road = np.where(mask==255)[0]
                    num = len(road)
                    if num < 200:
                        print("红色走完")
                        break
        self.stageLeft.remove(7)
        print("----楼梯关结束-----")
   
   
   
    def stage8(self):
        print("---终于是最后一关了------")
        self.MotionSet(v_x = 0.,v_y =0.0)    
        self.motors[19].setPosition(-0.2)  
        self.mGaitManager.step(self.mTimeStep)  # 步态生成器生成一个步长的动作
        self.myStep() 
        time = count(start=0,step=1)    
        self.motors[19].setPosition(-0.2)
        self.motors[18].setPosition(math.radians(80))
        time = count(start=0,step=1)     
        y_left = 0
        y_right = 0
        for n in time:
            self.motors[19].setPosition(-0.2)
            self.motors[18].setPosition(math.radians(80))
            self.mGaitManager.step(self.mTimeStep)
            self.myStep() 
            #   print(self.positionSensors[18].getValue())
            if n%5==0 and (np.abs(self.positionSensors[18].getValue()-math.radians(80))<0.1):
                image1 =sensorData.ImageGet(self.mCamera, self.mTimeStep)   #获取图片
                hsv = cv2.cvtColor(image1,cv2.COLOR_BGR2HSV)  #转化为hsv
                low = [100, 10, 100]
                high = [150, 80, 200]
                #np.array(self.currentStage ['hsv']['high'])
                hsv_gaussian = cv2.GaussianBlur(hsv, (3, 3), 0)                          
                mask=cv2.inRange(hsv,np.array(low),np.array(high))       
                mask = cv2.dilate(mask, np.ones((3, 3), np.uint8), iterations=2)
                mask = cv2.erode(mask, None, iterations=2)
                mask = cv2.medianBlur(mask,3)
                cv2.imwrite("/home/wenjing/View.png",mask)  
                # print(self.positionSensors[18].getValue())                        
                if len(np.where(mask == 255)[0]):  
                    area = np.where(mask==255)
                    y_left = area[0].min()
                # print(leftValue)
                while True:                                     
                    self.MotionSet(v_x = 0., v_y =0.)
                    self.StraightenRobot() 
                    self.mGaitManager.step(self.mTimeStep)  
                    self.myStep()          
                    if np.abs(self.angle[-1])>0.05:  
                        break                   
                if y_left > 15:
                    self.MotionSet(v_x = 0., v_y = -1.)
                    self.StraightenRobot() 
                    self.mGaitManager.step(self.mTimeStep)  
                    self.myStep()  
                elif y_left <= 15:
                    break
        self.motors[18].setPosition(math.radians(-80))
        time = count(start=0,step=1)    
        isWalk = False #是否可以走动
        BarDown = False
        DownTime = 0  #落下次数 
        for n in time:
            self.motors[18].setPosition(math.radians(-80))
            self.mGaitManager.step(self.mTimeStep)
            self.myStep() 
            #   print(self.positionSensors[18].getValue())
            if n%5 == 0 and (np.abs(self.positionSensors[18].getValue()+math.radians(80))<0.1):
                image1 =sensorData.ImageGet(self.mCamera, self.mTimeStep)   #获取图片
                hsv = cv2.cvtColor(image1,cv2.COLOR_BGR2HSV)  #转化为hsv
                low = [100, 10, 100]
                high = [150, 80, 200]
                #np.array(self.currentStage ['hsv']['high'])
                hsv_gaussian = cv2.GaussianBlur(hsv, (3, 3), 0)                          
                mask=cv2.inRange(hsv,np.array(low),np.array(high))     
                mask = cv2.dilate(mask, np.ones((3, 3), np.uint8), iterations=2)
                mask = cv2.erode(mask, None, iterations=2)
                mask = cv2.medianBlur(mask,3)
                cv2.imwrite("/home/wenjing/View.png",mask)  
                # print(self.positionSensors[18].getValue()) 
                if len(np.where(mask == 255)[0]):  
                    area = np.where(mask==255)
                    y_right = area[0].min()
                    print(y_right)
                # print(leftValue)  
                while True:                                     
                    self.MotionSet(v_x = 0., v_y =0.)
                    self.StraightenRobot() 
                    self.mGaitManager.step(self.mTimeStep)  
                    self.myStep()          
                    if np.abs(self.angle[-1])>0.05:  
                        break                 
                if y_right > 15 :
                    self.MotionSet(v_x = 0., v_y = 1.)
                    self.StraightenRobot()
                    self.mGaitManager.step(self.mTimeStep)  
                    self.myStep()  
                elif y_right <= 15 :
                    break    
                
         #识别黄色   
        print("---开始行走----")                   
        self.motors[18].setPosition(0)
        self.motors[19].setPosition(0) 
        self.MotionSet(v_x = 0., v_y = 0.)
        self.mGaitManager.step(self.mTimeStep)
        self.myStep()   
        while True:
                self.motors[18].setPosition(0)
                self.motors[19].setPosition(0)
                self.mGaitManager.step(self.mTimeStep)
                self.myStep() 
                if (np.abs(self.positionSensors[18].getValue())<0.1) and (np.abs(self.positionSensors[19].getValue())<0.05):
                    break  
        iterator = count(start=0,step=1) #迭代生成时间序列
        print("开始矫正")
        isWalk = False #是否可以走动
        BarkDown = False
        DownTime = 0  #落下次数
        for n in iterator:
        #开始前先矫正
            self.MotionSet(v_x = 0., v_y = 0.)
            self.mGaitManager.step(self.mTimeStep)
            self.myStep()
            if n % 5 == 0:
                print("正在矫正")
                self.MotionSet(v_x = 0., v_y = 0.)
                self.mGaitManager.step(self.mTimeStep)
                self.myStep()  
                self.StraightenRobot()
                self.mGaitManager.step(self.mTimeStep)  #这里得单独加上，不然不会刷新，会一直呆在原地
                self.myStep()
                image1 =sensorData.ImageGet(self.mCamera, self.mTimeStep)   #获取图片
                hsv = cv2.cvtColor(image1,cv2.COLOR_BGR2HSV)  #转化为hsv
                low = np.array([5,198,200])
                high = np.array([95,251,255])
                mask = cv2.inRange(hsv, low, high) #提取黄色
                cv2.imwrite('/home/wenjing/test.png', mask)
                obstacle = np.where(mask == 255)[0]
                num = len(obstacle)
                if num > 300:
                        DownTime = 1
                        BarDown = True
                     
                else:
                        BarDown = False
                print(DownTime)
                if DownTime == 0:
                    print("---横杆未落下，等待下一次开启----")
                    isWalk = False
                elif DownTime == 1:
                    if BarDown == True:
                        print("---横杆未开启,等待开启---")
                        isWalk = False
                    else:
                        print("----可以开始走动了-----")
                        isWalk = True
            if isWalk:
                print("调整速度!")
                self.MotionSet(v_x=1.,v_y =0.)
                break
            self.mGaitManager.step(self.mTimeStep)
            self.myStep()      
     #判断出关条件
        iterator = count(start=0,step=1) #迭代生成时间序列
        for n in iterator:
        #开始前先矫正
            self.StraightenRobot()
            self.mGaitManager.step(self.mTimeStep)
            self.myStep()
            if n % 5 == 0:     
                image1 =sensorData.ImageGet(self.mCamera, self.mTimeStep)
                hsv = cv2.cvtColor(image1,cv2.COLOR_BGR2HSV)
                low = [100, 10, 100]
                high = [150, 80, 200]
                mask=cv2.inRange(hsv[self.mCamera.getHeight()//2:,:],np.array(low),np.array(high))
                road = np.where(mask==255)[0]
                num = len(road)
                # 判定关卡结束条件，当前材料的hsv基本消失在视野中下段。或者行进步数超限1000。
                if num < 800:
                    self.stageLeft.remove(8)
                    print("-----最后结束-----")                
                    break
    
 ####判断转弯以及下一关
 #第一关不用判断
 #判断是否是桥
    def IsBrige(self, materialInfo):
       self.motors[19].setPosition(0.1)  
       while True:
          self.mGaitManager.step(self.mTimeStep)  # 步态生成器生成一个步长的动作
          self.myStep() 
          if np.abs(self.positionSensors[19].getValue()-0.1)<0.05: 
              break
       #选择视野上半部分
       image1 =sensorData.ImageGet(self.mCamera, self.mTimeStep)
       low = materialInfo['hsv']['low']
       high = materialInfo['hsv']['high']
       hsv = cv2.cvtColor(image1,cv2.COLOR_BGR2HSV)
       mask = cv2.inRange(hsv[:self.mCamera.getHeight()//3,:],np.array(low),np.array(high))
       mask = cv2.erode(mask, None, iterations=2)
       mask = cv2.medianBlur(mask,3)
       cv2.imwrite('/home/wenjing/test.png', mask)
       contours, hierarchy = cv2.findContours(mask,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
       if len(np.where(mask == 255)[1]):  
           area = np.where(mask==255)
           l = area[1].max()-area[1].min()
           print(l)
           print(self.mCamera.getWidth())
       if l >= 2/3*self.mCamera.getWidth() and 1 in self.stageLeft:
            return False
       elif l < 2/3*self.mCamera.getWidth() and 5 in self.stageLeft :
            return True 
  #判断是否地雷关：根据地雷
    def WhichOne(self):
        self.motors[19].setPosition(0.5)
        while True:
             self.motors[19].setPosition(0.5)
             self.mGaitManager.step(self.mTimeStep)
             self.myStep()
             if np.abs(self.positionSensors[19].getValue()-0.5)<0.05:
                 break
        img =sensorData.ImageGet(self.mCamera, self.mTimeStep)   #获取图片
        img_gray= cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)  #灰度化
        ret, binary_mine = cv2.threshold(img_gray, 10, 255, cv2.THRESH_BINARY_INV)
        cv2.imwrite("/home/wenjing/mine.png",binary_mine)
        contours, h = cv2.findContours(binary_mine, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)   
        num = 0
        max_area = 0
        if len(contours):
            for countour in contours:
                if cv2.contourArea(countour) > 5:
                    num+=1
                if  cv2.contourArea(countour) > max_area and tuple(countour[countour[:,:,1].argmin()][0])[1]<self.mCamera.getHeight()/2:
                    max_area = cv2.contourArea(countour)
            if num > 2 and max_area < 60  and 2 in self.stageLeft:
                 return 2
            elif 2>=num > 0 and 4 in self.stageLeft:
                 return 4
            else:
                 return 6
                
  #判断是否门：
    # def isDoor(self):          
        # self.motors[19].setPosition(0.5)
        # while True:
             # self.motors[19].setPosition(0.5)
             # self.mGaitManager.step(self.mTimeStep)
             # self.myStep()
             # if np.abs(self.motors[19]-0.5)<0.05:
                 # break
        # img =sensorData.ImageGet(self.mCamera, self.mTimeStep)   #获取图片
        # img_gray= cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)  #灰度化
        # ret, binary_mine = cv2.threshold(img_gray, 10, 255, cv2.THRESH_BINARY_INV)
        # cv2.imwrite("/home/wenjing/bianary.png",binary_mine)
        # contours, h = cv2.findContours(binary_mine, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)   
        # num = 0
        # max_area = 0
        # if len(contours):
            # for count in countours:
                # if cv2.contourArea(count) > 5:
                    # num+=1
                # if  cv2.contourArea(count) > max_area:
                    # area = cv2.contourArea(count)
            # if max_area >= 125 and 4 in self.stageLeft:
                 # return True
            # else:
                 # return False
   #判断是否踢球
   #如果前两个函数均为False且6还在
                                   
    #判断是否转弯
    def JudgeCorner(self, materialInfo):
    #思路：观察材料长度,较短为弯路        
        if self.corner < 2:
            self.motors[19].setPosition(0.5)
            self.mGaitManager.step(self.mTimeStep)
            self.myStep()        
            while True:
                self.motors[19].setPosition(0.5)
                self.mGaitManager.step(self.mTimeStep)
                self.myStep()
                if np.abs(self.positionSensors[19].getValue()-0.5)<0.05:
                    break
            image = sensorData.ImageGet(self.mCamera, self.mTimeStep)
            hsv = cv2.cvtColor(image,cv2.COLOR_BGR2HSV) 
            hsv_gaussian = cv2.GaussianBlur(hsv, (3, 3), 0)                          
            low = materialInfo['hsv']['low']
            high = materialInfo['hsv']['high']                
            mask=cv2.inRange(hsv,np.array(low),np.array(high))       
            mask = cv2.dilate(mask, np.ones((3, 3), np.uint8), iterations=2)
            mask = cv2.erode(mask, None, iterations=2)
            mask = cv2.medianBlur(mask,3)
            contours, hierarchy = cv2.findContours(mask,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
            # 找面积最大的轮廓
            if len(contours):
                area = (max(contours, key=cv2.contourArea)).reshape(-1,2)
                x_min,y_min = np.min(area,axis=0)
            # 若轮廓上界在视野下方，则判断不是完整长形，要转弯
                if y_min > self.mCamera.getHeight()/2:
                    print("需要转弯")
                    return True
                else:
                    print("不用转弯")
                    return False 
        else:
              return False      
#判断在哪个关卡：排除法                
   # def StateNext(self):
    #先是花色        

#判断花色            
    def floorJudge(self,image):            
        self.motors[19].setPosition(0)           
        self.mGaitManager.step(self.mTimeStep)
        self.myStep()  
        cv2.imwrite('/home/wenjing/view.png', image)
        hsv = cv2.cvtColor(image,cv2.COLOR_BGR2HSV) 
        hsv_gaussian = cv2.GaussianBlur(hsv, (3, 3), 0)
        #遍历判断  
        max = 0      
        for info in raceMaterialInfo:
            low =  info['hsv']['low']
            high = info ['hsv']['high']   
            mask=cv2.inRange(hsv,np.array(low),np.array(high)) 
            cv2.imwrite('/home/wenjing/view1.png', mask)
            road = np.where(mask==255)[0] 
            num = len(road) 
            if num > max:
                current = info
                max = num #选择最接近的 
        return current           
 
 #   def findNextStage(self):
 #判断障碍物
    def ObstacleDetect(self,img):      
           flag = False
           hsv = cv2.cvtColor(img,cv2.COLOR_BGR2HSV)  #转化为hsv
           low = np.array([110,250,50]) #np.array(self.currentStage ['hsv']['low'])
           high = np.array([130,255,255])
           mask=cv2.inRange(hsv,low,high)                  
           mask = cv2.dilate(mask, np.ones((3, 3), np.uint8), iterations=2)
           mask = cv2.erode(mask, None, iterations=2) 
           contours,_ = cv2.findContours(mask,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE) 
           if len(contours):
		#最大蓝色轮廓
                      area = max(contours, key=cv2.contourArea)
		#寻找最小外接矩形并提取上面两个点坐标的中心点
                      if cv2.contourArea(area):
                                rect = cv2.minAreaRect(area)
                                box = cv2.boxPoints(rect)
                                box = np.int0(box)
                                print(box)
                                obstacle_x = (box[2][0]+box[3][0])/2
                                obstacle_y = (box[2][1]+box[3][1])/2
                                flag = True
                                return obstacle_x, obstacle_y,flag
                      else:
                                obstacle_x = -1
                                obstacle_y = -1
                                return obstacle_x, obstacle_y,flag
           else:
                    obstacle_x = -1
                    obstacle_y = -1  
                    return obstacle_x, obstacle_y,flag       
    def ObstacleFind(self,MaterialInfo):
           self.motors[19].setPosition(0.5)
           while True:
                self.motors[19].setPosition(0.5)
                self.mGaitManager.step(self.mTimeStep)
                self.myStep()
                if np.abs(self.positionSensors[19].getValue()-0.5)<0.05:
                    break         
           flag = False
           img= sensorData.ImageGet(self.mCamera, self.mTimeStep)
           hsv = cv2.cvtColor(img,cv2.COLOR_BGR2HSV)  #转化为hsv
           low = np.array([110,250,50]) #np.array(self.currentStage ['hsv']['low'])
           high = np.array([130,255,255])
           mask=cv2.inRange(hsv,low,high)                  
           mask = cv2.dilate(mask, np.ones((3, 3), np.uint8), iterations=2)
           mask = cv2.erode(mask, None, iterations=2) 
           contours,_ = cv2.findContours(mask,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE) 
           if len(contours):
		#最大蓝色轮廓
                      area = max(contours, key=cv2.contourArea)
		#寻找最小外接矩形并提取上面两个点坐标的中心点
                      if cv2.contourArea(area):
                                rect = cv2.minAreaRect(area)
                                box = cv2.boxPoints(rect)
                                box = np.int0(box)
                                print(box)
                                obstacle_x = (box[2][0]+box[3][0])/2
                                obstacle_y = (box[2][1]+box[3][1])/2
                                flag = True
                                return obstacle_x, obstacle_y,flag
                      else:
                                obstacle_x = -1
                                obstacle_y = -1
                                return obstacle_x, obstacle_y,flag
           else:
                    obstacle_x = -1
                    obstacle_y = -1  
                    return obstacle_x, obstacle_y,flag         
#判断楼梯关，检验红色
    def IsStairs(self):
        self.motors[19].setPosition(0.5)
        print("判断是否为楼梯关")
        while True:
              self.motors[19].setPosition(0.5)
              self.mGaitManager.step(self.mTimeStep)
              self.myStep()
              if np.abs(self.positionSensors[19].getValue()-0.5)<0.05:
                 break 
        image = sensorData.ImageGet(self.mCamera, self.mTimeStep)
        low =  [0,180,205] # 红色
        high = [255,255,255]
        hsv = cv2.cvtColor(image,cv2.COLOR_BGR2HSV)
        mask=cv2.inRange(hsv,np.array(low),np.array(high))
        kernel = cv2.getStructuringElement(cv2.MORPH_RECT,(1,1))
        mask = cv2.morphologyEx(mask, cv2.MORPH_OPEN, kernel,iterations=3)
        contours, hierarchy = cv2.findContours(mask,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
        num = 0
        # 不要用len(contours)，原因是可能有噪点。
        for contour in contours:           
            if cv2.contourArea(contour) > 20:
                num += 1
        if num >= 2:  #和楼梯在同一直线也会看到
            return True
        else:
            return False     
 #还剩下最后一关，为末尾关       
 #定义关卡：0-绿地上下开合栏杆 1-绿色回字陷阱 2-灰(棕、白)色扫雷关 3-蓝白棕地障碍物 4-黑色棕(白、灰)地窄门 5-绿色独木桥 6-白(棕、灰)底踢球进洞 7-蓝绿红台阶 8-水平开合栏杆(花纹地)
#地雷(2)、门(4)、球(6)三关地面会交换
 #判断下一关是那一关
    def StageNext(self):
    #先进行花色判断
        res = {'stage_num':-1,'turn_flag':False,'obstacle_flag':False}
        self.motors[19].setPosition(-0.2)    
        while True:
              self.motors[19].setPosition(-0.2)
              self.mGaitManager.step(self.mTimeStep)
              self.myStep()
              if np.abs(self.positionSensors[19].getValue()+0.2)<0.05:
                 break      
        image = sensorData.ImageGet(self.mCamera, self.mTimeStep)
        MaterialInfo =  self.floorJudge(image) 
        print(MaterialInfo)
        self.materialInfo = MaterialInfo  
        print(res)
        if  MaterialInfo['material'] == 'green': #可能为1，5    
            if (self.IsBrige(self.materialInfo) and 5 in  self.stageLeft):  
                self.stagenow = 5
            elif 1 in  self.stageLeft:
                self.stagenow = 1
            res['stage_num'] = self.stagenow
        elif  MaterialInfo['material'] == 'white' or   MaterialInfo['material']  == 'grey':
            if self.JudgeCorner(self.materialInfo):
               res['turn_flag'] = True
               print("转弯")   
            else:  
               self.stagenow =  self.WhichOne()   
               print(self.stagenow)
               x,y,flag = self.ObstacleFind(MaterialInfo)
               if flag == True:
                    res['obstacle_flag'] = True
                    print('结束后有障碍物')   
        elif MaterialInfo['material'] == 'yellow_brick':
        #可能为楼梯关，判断
            print("黄色砖块路况判断")
            flag3 = self.IsStairs()
            flag4 = self.JudgeCorner(MaterialInfo)
            print(flag3)
            if flag3 and 7 in self.stageLeft: 
                self.stagenow = 7
            else:
                if self.JudgeCorner(MaterialInfo):
                    res['turn_flag'] = True
                    print('转弯')
                else:
                    self.stagenow =  self.WhichOne()  
                    print(self.stagenow)   
                    res['stage_num'] = self.stagenow
                    x,y,flag = self.ObstacleFind(MaterialInfo)
                    if flag == True:
                        res['obstacle_flag'] = True
                        print('结束后有障碍物')            
        else:
             #如果剩下1关，为最后关
             if len(self.stageLeft) == 1:
                 self.stagenow = 8
             elif len(self.stageLeft) == 9:
                 self.stagenow = 0     
        res['stage_num'] = self.stagenow
        print(res) 
        return res

    def turnLeft(self):
        print("准备转弯")
        #判断下一关是否有距离较近的地雷
        if 2 in self.stageLeft:
            self.MotionSet(v_x=0.0)
            self.motors[19].setPosition(-0.2)
            while np.abs(self.positionSensors[19].getValue()+0.2)>0.05:
                img = sensorData.ImageGet(self.mCamera, self.mTimeStep)
                self.mGaitManager.step(self.mTimeStep)  # 步态生成器生成一个步长的动作
                self.myStep()  # 仿真一个步长
                img_gray= cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)  #灰度化
                ret, binary_mine = cv2.threshold(img_gray, 10, 255, cv2.THRESH_BINARY_INV)
                cv2.imwrite("/home/wenjing/mine.png",binary_mine)
                contours, h = cv2.findContours(binary_mine, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)   
                num = 0
                mine = []
                if len(contours):
                    for counter in contours:
                        if cv2.contourArea(counter) > 5:
                            num+=1
                            M = cv2.moments(counter)  
                            center_x = int(M["m10"] / M["m00"])
                            center_y = int(M["m01"] / M["m00"])
                            if 20 < center_x < 120:
                                mine.append((center_x,center_y))
                if len(mine):
                    while np.abs(self.angle[-1]+45) > 1:
                        change_speed = -1024/32000 * (self.angle[-1]+45)
                        change_speed = rangeLimit(change_speed, 1,-1)
                        self.MotionSet(self.vx,self.vy, change_speed)
                        self.mGaitManager.step(self.mTimeStep)
                        self.myStep()
                    break 
                else:
                    print('无雷')
        
        # 正常转弯程序
        self.motors[19].setPosition(0.2)
        time = count(start=0,step=1)
        for n in time:
            change_speed = -1024/32000 * self.angle[-1]
            change_speed = rangeLimit(change_speed, 1,-1)
            self.MotionSet(v_x = 1,v_a=change_speed)
            self.mGaitManager.step(self.mTimeStep)  # 步态生成器生成一个步长的动作
            self.myStep()  # 仿真一个步长
            if n % 5 == 0 and np.abs(self.positionSensors[19].getValue()-0.2)<0.05:
                image1 =sensorData.ImageGet(self.mCamera, self.mTimeStep)   #获取图片
                hsv = cv2.cvtColor(image1,cv2.COLOR_BGR2HSV)  #转化为hsv
                low = np.array(self.materialInfo ['hsv']['low'])
                high = np.array(self.materialInfo ['hsv']['high'])
                image_half = hsv[self.mCamera.getHeight()//2:,:]   
                mask=cv2.inRange(image_half,low,high)
                road = np.where(mask==255)[0]
                num = len(road)
                if num < 500:
                    break
        print("开始转弯")
        self.MotionSet(v_x=0.0)
        self.mGaitManager.step(self.mTimeStep)  # 步态生成器生成一个步长的动作
        self.myStep()
        iterator = count(start=0,step=1) #迭代生成时间序列
        self.angle[-1] = self.angle[-1]-90
        self.StraightenRobot()
        self.mGaitManager.step(self.mTimeStep)  # 步态生成器生成一个步长的动作
        self.myStep()       
        self.corner += 1
        print("转弯结束")        
    
    def passBrick(self,img):
    #假设楼梯关已经过了
        if 7 not in self.stageLeft:
            hsv = cv2.cvtColor(img,cv2.COLOR_BGR2HSV)
            low = raceMaterialInfo[2]['hsv']['low'] # 黄色砖块
            high = raceMaterialInfo[2]['hsv']['high']
            mask=cv2.inRange(hsv[self.mCamera.getHeight()//2:,:],np.array(low),np.array(high))
            road = np.where(mask==255)[0]
            num = len(road)
            if num < 250:
                return True
            else:
                return False
#楼梯存在
        else:
            hsv = cv2.cvtColor(img,cv2.COLOR_BGR2HSV)
            low =  raceMaterialInfo[2]['hsv']['low'] # 黄色砖块
            high = raceMaterialInfo[2]['hsv']['high']
            mask=cv2.inRange(hsv[self.mCamera.getHeight()//2:,:],np.array(low),np.array(high))
            road = np.where(mask==255)[0]
            num = len(road)
            if num < 250:
                flag = True
            else:
                flag = False
            img = img[90:120][:][:]
            img = cv2.resize(img, (img.shape[1]*4, img.shape[0] * 4), cv2.INTER_NEAREST)
            gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
            dst = cv2.Canny(gray, 40, 100, apertureSize=3)
            k = np.ones((8, 8), np.uint8)
            close = cv2.dilate(dst, k, iterations=2)
            close = cv2.erode(close,k,iterations=1)
            close=~close
            contours = cv2.findContours(close.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[-2]
            cnt=0
            for contour in contours:
                rect = cv2.minAreaRect(contour)
                box = cv2.boxPoints(rect)
                area = cv2.contourArea(box)
                if 1000 < area < 5000:
                    cnt+=1    
            if self.corner ==0:
                if cnt > 9:
                    flag2 = True
                else:
                    flag2 = False
            else:
                if cnt > 15:
                    flag2 = True
                else:
                    flag2 = False
            if flag or flag2:
                return True
                    

   
    def stop(self):
        self.setMoveCommand(vX=0.)
        time = repeat(50)
        for n in time:
            self.mGaitManager.step(self.mTimeStep)
            self.myStep()
        self.mGaitManager.stop()
        self.wait(200)   
             
    def run(self): 
        self.start()  
        strategies = {
            0 : self.stage0,
            1 : self.stage1,
            2 : self.stage2,
            3 : self.stage3,
            4 : self.stage4,
            5 : self.stage5,
            6 : self.stage6,
            7 : self.stage7,
            8 : self.stage8,
        }
        
        while len(self.stageLeft):
            strategies[self.stagenow]()  #选择关卡
            #判断关卡
            print(self.stageLeft)
            print("开始判断关卡")
            res = self.StageNext()
            if res['turn_flag']:
                self.turnLeft()
                res = self.StageNext()
            self.isObstacle = res['obstacle_flag']
        self.stop()
        while True:
            self.mMotionManager.playPage(24)


if __name__ == '__main__':
    walk = Walk()  # 初始化Walk类
    walk.run()  # 运行控制器
